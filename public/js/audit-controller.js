/**
 * Created by AnkurR on Jan 010 10/01/2017.
 * console.log(JSON.stringify($scope.accessList));
 */
TopengUI
    .controller('AuditController', function($scope, $rootScope, AuditService, UserService) {
        $rootScope.menuItem = 'audit';
        $rootScope.pageTitle = 'Audit Log';
        $rootScope.controllerSubMenuLogs = true;
        $scope.auditList = [];
        $scope.page = 1;
        $scope.noPrevious = true;
        $scope.noNext = true;
        $scope.filter = {};
        UserService.get({}, function(success) {
            if(success.success) {
                $scope.userList = success.data;
            }
        });
        
        $scope.loadAudits = function(page) {
            $scope.filter.page = page;
            $scope.showProgress = true;
            AuditService.get($scope.filter, function(success) {
                $scope.showProgress = false;
                if(success.success) {
                    if(success.data.length == 0) {
                        $scope.page -= 1;
                    }
                    $scope.noNext = success.data.length < 10;
                    $scope.auditList = success.data;
                }
            }, function(err) {
                $scope.showProgress = false;
                console.log(err);
            });
        };
        $scope.callNextPage = function(currentPage) {
            $scope.page += currentPage;
            if($scope.page > 1) {
                $scope.noPrevious = false;
            } else if($scope.page == 1) {
                $scope.noPrevious = true;
            }
            if($scope.page > 0) {
                $scope.loadAudits($scope.page);
            } else {
                $scope.page = 1;
            }
        };
        $scope.loadAudits(1);
        $scope.doFilter = function(formValue) {
            if(formValue == undefined) {
                formValue = "all";
            }
            if(formValue) {
                $scope.filter = {};
                var filter = {};
                if(formValue.userid) {
                    filter.userid = formValue.userid;
                }
                if(formValue.userid) {
                    $scope.filter.userid = formValue.userid;
                }
                if(formValue.userid == "all") {
                    $scope.filter = {};
                }
                $scope.page = 1;
                $scope.callNextPage(0);
            }
        };
    })
    .controller('AccessLogController', function($scope, $rootScope, AccessLogService, UserService) {
        $rootScope.menuItem = 'accesslog';
        $rootScope.pageTitle = 'Access Log';
        $rootScope.controllerSubMenuLogs = true;
        $scope.accessList = [];
        $scope.page = 1;
        $scope.noPrevious = true;
        $scope.noNext = true;
        $scope.filter = {
            auth : '0',
            userid : 'all'
        };
        UserService.get({}, function(success) {
            if(success.success) {
                $scope.userList = success.data;
            }
        });
        
        $scope.loadAccessLogs = function(page) {
            $scope.filter.page = page;
            $scope.showProgress = true;
            AccessLogService.get($scope.filter, function(success) {
                $scope.showProgress = false;
                if(success.success) {

                    if(success.data.length == 0) {
                        $scope.page -= 1;
                    }
                    $scope.noNext = success.data.length < 50;
                    $scope.accessList = success.data;
                }
            }, function(err) {
                $scope.showProgress = false;
                console.log(err);
            });
        };
        $scope.callNextPage = function(currentPage) {
            $scope.page += currentPage;
            if($scope.page > 1) {
                $scope.noPrevious = false;
            } else if($scope.page == 1) {
                $scope.noPrevious = true;
            }
            if($scope.page > 0) {
                $scope.loadAccessLogs($scope.page);
            } else {
                $scope.page = 1;
            }
        };
        $scope.loadAccessLogs(1);
        $scope.callNextPage(0);
        $scope.doFilter = function() {
            $scope.page = 1;
            $scope.callNextPage(0);
        };
    })
    .controller('PolicyLogController', function($scope, $rootScope, PolicyLogService, UserService) {
        $rootScope.menuItem = 'policylog';
        $rootScope.pageTitle = 'Policy Violation Log';
        $rootScope.controllerSubMenuLogs = true;
        $scope.policyLogList = [];
        $scope.page = 1;
        $scope.noPrevious = true;
        $scope.noNext = true;
        $scope.filter = {
            userid : 'all'
        };
        UserService.get({}, function(success) {
            if(success.success) {
                $scope.userList = success.data;
            }
        });
        $scope.loadAccessLogs = function(page) {
            $scope.filter.page = page;
            $scope.showProgress = true;
            PolicyLogService.get($scope.filter, function(success) {
                $scope.showProgress = false;
                if(success.success) {
                    if(success.data.length == 0) {
                        $scope.page -= 1;
                    }
                    $scope.noNext = success.data.length < 50;
                    $scope.policyLogList = success.data;
                }
            }, function(err) {
                $scope.showProgress = false;
                console.log(err);
            });
        };
        $scope.callNextPage = function(currentPage) {
            $scope.page += currentPage;
            if($scope.page > 1) {
                $scope.noPrevious = false;
            } else if($scope.page == 1) {
                $scope.noPrevious = true;
            }
            if($scope.page > 0) {
                $scope.loadAccessLogs($scope.page);
            } else {
                $scope.page = 1;
            }
        };
        $scope.callNextPage(0);
        $scope.doFilter = function() {
            $scope.page = 1;
            $scope.callNextPage(0);
        };
    })