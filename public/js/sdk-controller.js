TopengUI.controller('SDKController', function($scope, $rootScope, SDKService, $stateParams, $state, $timeout) {
    $rootScope.menuItem = 'sdkmgmt';
    $rootScope.pageTitle = 'SDK Management';
    $scope.showProgress = true;
	$scope.sdklistData = {};
	$scope.sdkDatas = {};
    $scope.listSdks = function() {
        $scope.showProgress = false;
        SDKService.get({}, function(success) {
            if(success.success) {
				//alert (JSON.stringify(success.data));
                $scope.sdkList = success.data;
            }
            console.log(success);
        })
    };
    $scope.listSdks();
    $scope.updateSDK = function(sdk) {
        var isUnique = true;
        $scope.sdkList.forEach(function(data) {
            if(sdk.appName == data.appName && sdk.moduleId != data.moduleId) {
                isUnique = false;
                return false;
            }
        });
        if(!isUnique) {
            sdk.errorText = 'App name is already exist';
			alert("App name is already exist");
            return false;
        }
        var sdkData = new SDKService();
        sdkData.data = {
            action : 'update',
            module : sdk.module,
            moduleId : sdk.moduleId,
            name : sdk.appName
        };
        $scope.showProgress = true;
        sdkData.$save(function(success) {
            $scope.showProgress = false;
            if(success.success) {
                sdk.editing = false;
				$scope.isEdit = false;
                $scope.listSdks();
				
				$state.go('sdk_management', null, { reload: true });
            }
        });
    };
	
	$scope.isEdit = true;
        $scope.showEditFormData = function(sdk) {
            $scope.editId =  sdk.moduleId; //editId;
            $scope.isEdit = true;
            $scope.showProgress = true;
			//alert (JSON.stringify($scope.editId)); 
            SDKService.get({moduleId : $scope.editId,}, function(success) {
                $scope.showProgress = false;
				//alert (JSON.stringify(success)); 
                if(success.success) {
					$scope.sdklistData = success.data;
					var keepGoing = true;
					//alert (JSON.stringify($scope.sdklistData));
                    $scope.sdklistData.forEach(function(data) {
						if(keepGoing) {
							if(sdk.moduleId == data.moduleId) {
								$scope.sdkDatas = data;
								//alert (JSON.stringify($scope.sdkDatas));
								keepGoing  = false;
							}
						}
					});
                    angular.element(document.getElementsByClassName('is-invalid')).addClass('is-dirty').removeClass('is-invalid');
                    $scope.toggleAddBox();
                }
            }, function(error) {
                $scope.showProgress = false;
                console.log(error);
                alert('Unable to get data from server');
                $state.go('sdk_management', null, { reload: true });
            }); 
        };
		
		$scope.submitAddSdk = function(formData) {
            $scope.showProgressAddUpdate = true;
            formData.id = $scope.editId;
            $scope.sdkService = new SdkService();
            $scope.sdkService.data = formData;
            if($scope.isEdit) {
                $scope.sdkService.moduleId = $scope.editId;
                $scope.sdkService.$update(function(success) {
                    $scope.showProgressAddUpdate = false;
                    console.log(success);
                    if(success.success) {
                        $scope.toggleAddBox('cancel');
                        $timeout(function() {
                            $scope.loadSdks(1);
                        }, 200);
                    }
                }, function(error) {
                    $scope.showProgressAddUpdate = false;
                    console.error(error);
                    alert('Unable to get data from server');
                });
            } else {
                $scope.sdkService.$save(function(success) {
                    $scope.showProgressAddUpdate = false;
                    console.log(success);
                    if(success.success) {
                        $scope.toggleAddBox('cancel');
                        $timeout(function() {
                            $scope.loadSdks(1);
                        }, 200);
                    }
                }, function(error) {
                    $scope.showProgressAddUpdate = false;
                    console.error(error);
                    alert('Unable to get data from server');
                });
            }
        };
		
		
		
		/// load popup
		$scope.toggleAddBox = function(source) {
            if(source == 'cancel') {
                $scope.sdklistData = {};
                $timeout(function() {
                    angular.forEach(angular.element(document.getElementsByClassName('ng-invalid')), function(el) {
                        if(el.tagName.toLowerCase() != 'form') {
                            angular.element(el).parent().addClass('is-invalid').removeClass('is-dirty');
                        }
                    });
                }, 200);
            } else if(source == 'add') {
                $scope.isEdit = false;
            }
            $rootScope.overFlow = !$scope.addSdkToggle;
            $scope.addSdkToggle = !$scope.addSdkToggle;
        };
		
		 $scope.loadSdks = function(pageNo) {
            $scope.showProgress = true;
            SdkService.get({ page: pageNo}, function(success) {
                $scope.showProgress = false;
                if(success.success) {
                    if(success.data.length == 0) {
                        $scope.page -= 1;
                    }
                    $scope.noNext = success.data.length < parseInt(localStorage.getItem('pageLimit') || 10);
                    $scope.sdklist = success;
                }
            }, function(error) {
                $scope.showProgress = false;
                console.error(error);
                alert('Unable to get data from server');
            });
        };
		
		$scope.deleteSDK = function(sdk) {
			var sdkData = new SDKService();
			sdkData.data = {
				action : 'delete',
				module : sdk.module,
				moduleId : sdk.moduleId
			};
			sdkData.$save(function(success) {
				if(success.success) {
					$scope.listSdks();
				}
			});
		};
})