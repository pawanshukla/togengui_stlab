var TopengUI;
(function(angular) {
    TopengUI = angular.module('TopengUI', ['ui.router', 'ngResource', 'ui.bootstrap', 'ngCookies', 'autocomplete', 'chart.js', 'angular.filter', 'ng-ip-address'])
        .config(function ($httpProvider, ChartJsProvider) {
            $httpProvider.interceptors.push('httpLoaderInterceptor');
            ChartJsProvider.setOptions({ colors : [ '#803690', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360' ] });
        })
        .run(function ($rootScope, $location, $http, $timeout, Logout) {
            $rootScope.$on('$viewContentLoaded', function() {
                $timeout(function() {
                    componentHandler.upgradeAllRegistered();
                });
            });
            $rootScope.maxPagingSize = 5;
            var history = [];
            $rootScope.$on('$routeChangeSuccess', function() {
                if($location.path() === "/") {
                    history = [];
                }
                history.push($location.path());
            });
            $rootScope.back = function () {
                var prevUrl = history.length > 1 ? history.splice(-2)[0] : "/";
                $location.path(prevUrl);
            };
            $rootScope.doLogout = function() {
                Logout.doLogout();
            };
            $rootScope.username = window.localStorage.getItem('username');
            $rootScope.validatePhone = function(str, element) {
                if(str && (''+str).length == 10) {
                    // const regex = /\d{10}$/g;
                    const regex = /^\+?\d{2}|\0(?:\-?|\ ?)(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$/g;
                    element.$invalidFormat = (regex.exec(str) == null);
                }
            };
            $rootScope.validateAlphaDotString = function(str, element) {
                if(str && ('' + str).length > 0) {
                    const regex = /^[a-zA-Z .]+$/g;
                    element.$invalidFormat = (regex.exec(str) == null);
                }
            };
            $rootScope.validateAlphaNumDotString = function(str, element) {
                if(str && ('' + str).length > 0) {
                    const regex = /^[a-zA-Z0-9 .]+$/g;
                    element.$invalidFormat = (regex.exec(str) == null);
                }
            };
            $rootScope.validateAlphaNumString = function(str, element) {
                if(str && ('' + str).length > 0) {
                    const regex = /^[a-zA-Z0-9]+$/g;
                    element.$invalidFormat = (regex.exec(str) == null);
                }
            };
            $rootScope.validateNumDotString = function(str, element) {
                if(str && ('' + str).length > 0) {
                    const regex = /^[0-9.]+$/g;
                    element.$invalidFormat = (regex.exec(str) == null);
                }
            };
            $rootScope.validateAlphaString = function(str, element) {
                if(str && ('' + str).length > 0) {
                    const regex = /^[a-zA-Z -]+$/g;
                    element.$invalidFormat = (regex.exec(str) == null);
                }
            };
        })
        .directive("fileread", [function () {
            return {
                scope: {
                    fileread: "="
                },
                link: function (scope, element, attributes) {
                    element.bind("change", function (changeEvent) {
                        scope.$apply(function () {
                            if(element.attr('multiple')) {
                                scope.fileread = changeEvent.target.files;
                            } else {
                                scope.fileread = changeEvent.target.files[0];
                            }
                        });
                    });
                }
            }
        }]);
    TopengUI.constant('API_URL', '/api/');
})(window.angular);
