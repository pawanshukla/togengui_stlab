TopengUI
    .controller('StandardsController', function($scope, $rootScope, $stateParams, $state, StandardService, $timeout) {
        $rootScope.menuItem = 'standard';
        $rootScope.pageTitle = 'Standards';
        $scope.policies = [];
        $scope.selectedPolicy = [];
        $scope.showEditFormData = function(stdId) {
            $scope.showProgress = true;
            StandardService.get({standardId : stdId}, function(success) {
                $scope.showProgress = false;
                //console.log(success);
                if(success.success) {
                    $scope.editId = stdId;
                    $scope.isEdit = true;
                    $scope.standardData = success.data;
                    angular.element(document.getElementsByClassName('is-invalid')).addClass('is-dirty').removeClass('is-invalid');
                    $scope.toggleAddBox();
                }
            }, function(error) {
                $scope.showProgress = false;
                console.log(error);
                alert('Unable to get data from server');
                $state.go('standards', null, { reload: true });
            });
        };
        $scope.submitAddStandard = function(formData) {
            formData.id = $scope.editId;
            $scope.standardService = new StandardService();
            $scope.standardService.data = formData;
            $scope.showProgressAddUpdate = true;
            $scope.errorMessage = [];
            if($scope.isEdit && $scope.editId) {
                $scope.standardService.standardId = $scope.editId;
                $scope.standardService.$update(function(success) {
                    $scope.showProgressAddUpdate = false;
                    console.log(success);
                    if(success.success) {
                        $scope.toggleAddBox('cancel');
                        $timeout(function() {
                            $scope.callNextPage(0);
                        });
                    } else {
                        $scope.errorMessage = success.errMessage;
                    }
                }, function(error) {
                    $scope.showProgressAddUpdate = false;
                    console.error(error);
                    alert('Unable to get data from server');
                });
            } else {
                $scope.standardService.$save(function(success) {
                    $scope.showProgressAddUpdate = false;
                    console.log(success);
                    if(success.success) {
                        $scope.toggleAddBox('cancel');
                        $timeout(function() {
                            $scope.callNextPage(0);
                        });
                    } else {
                        $scope.errorMessage = success.errMessage;
                    }
                }, function(error) {
                    $scope.showProgressAddUpdate = false;
                    console.error(error);
                    alert('Unable to get data from server');
                });
            }
        };

        /* Standard Controller */
        $scope.addStandardToggle = false;
        $scope.page = 1;
        $scope.noPrevious = true;
        $scope.noNext = false;
        $scope.toggleAddBox = function(source) {
            if(source == 'cancel') {
                $scope.standardData = {};
                $timeout(function() {
                    angular.forEach(angular.element(document.getElementsByClassName('ng-invalid')), function(el) {
                        if(el.tagName.toLowerCase() != 'form') {
                            angular.element(el).parent().addClass('is-invalid').removeClass('is-dirty');
                        }
                    });
                }, 200);
            } else if(source == 'add') {
                $scope.isEdit = false;
            }
            $rootScope.overFlow = !$scope.addStandardToggle;
            $scope.addStandardToggle = !$scope.addStandardToggle;
        };
        $scope.loadStandards = function(pageNo) {
            $scope.showProgress = true;
            StandardService.get({ page: pageNo}, function(success) {
                $scope.showProgress = false;
                if(success.success) {
                    if(success.data.length == 0) {
                        $scope.page -= 1;
                    }
                    $scope.noNext = success.data.standardList.length < parseInt(localStorage.getItem('pageLimit') || 10);
                    $scope.standardlist = success.data.standardList;
                }
            }, function(error) {
                $scope.showProgress = false;
                console.error(error);
                alert('Unable to get data from server');
            });
        };
        $scope.callNextPage = function(currentPage) {
            $scope.page += currentPage;
            if($scope.page > 1) {
                $scope.noPrevious = false;
            } else if($scope.page <= 1) {
                $scope.noPrevious = true;
            }
            if($scope.page > 0) {
                $scope.loadStandards($scope.page);
            } else {
                $scope.page = 1;
            }
        };
        $scope.callNextPage(0);
        $scope.deleteStandard = function(standardid, event) {
            var deleteElement = angular.element(event.target).parent().parent().parent();
            if(confirm('Are you sure you want to delete standard?')) {
                $scope.showProgress = true;
                $scope.standardService = new StandardService();
                $scope.standardService.standardId = standardid;
                $scope.standardService.$delete(function(success) {
                    $scope.showProgress = false;
                    if(success.success) {
                        deleteElement.remove();
                    }
                }, function(error) {
                    $scope.showProgress = false;
                    alert('Unable to get data from server');
                });
            }
        }
    })