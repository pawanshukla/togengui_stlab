TopengUI.controller('ImportLdapController', function($scope, $rootScope, DataService) {
    $rootScope.menuItem = 'importldap';
    $rootScope.controllerSubMenu2 = true;
    $rootScope.pageTitle = 'Import From LDAP';

    $scope.submitImportData = function(data) {
        console.log(data);
        $scope.importData = {};
        $scope.showProgress = true;
        var dataService = new DataService();
        dataService.data = data;
        dataService.$save(function(data) {
            $scope.showProgress = false;
            if(data.success) {
                $scope.importData = {};
            }
        });
    };
});