TopengUI
    .controller('LoginController', function($scope, $rootScope, Login) {
        $rootScope.pageTitle = 'Topeng UI - Login';
        $rootScope.menuItem = 'login';
        $scope.submitLogin = function(formData) {
            console.log(formData);
            Login.then(function(data) {
                $rootScope.response= data;
                console.info(data);
            }, function(error) {
                $rootScope.response= error;
                console.log(1, error);
            });
        };
        $scope.submitSetPassword = function(formData) {
            console.log(formData);
        };
    })
    .controller('ConfigController', function($scope, $rootScope) {
        $rootScope.menuItem = 'config';
        $rootScope.pageTitle = 'User configurations';
    });