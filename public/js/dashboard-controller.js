TopengUI
    .controller('DashboardController', function($scope, $rootScope, DashboardService, $interval, $timeout, AccessLogService, PolicyLogService) {
        $rootScope.menuItem = 'dashboard';
        $rootScope.pageTitle = 'Dashboard';
        $scope.filterData = {daterange : 'Past Week'};
        $scope.labels = {};
        $scope.data = {};
        $scope.options = {};
        $scope.series = {};
        $scope.datasetOverride = {};
        $scope.stats = {};
        DashboardService.get({}, function(data) {
            console.log(data);
        }, function(err) {
            console.log(err);
        });
        $timeout(function() {
            angular.element(document.getElementsByClassName('mdl-textfield--floating-label')).addClass('is-dirty');
        }, 200);
        $scope.performanceModuleFilter = 'DSWC';
        $scope.processTopAppDataChart = function (data) {
            $scope.labels['pieTopAppData'] = [];
            $scope.data['pieTopAppData'] = [];
            data.forEach(function(val) {
                $scope.labels['pieTopAppData'].push(val._id);
                $scope.data['pieTopAppData'].push(val.count);
            });
            $scope.options['pieTopAppData'] = {
                title: {
                    display: false,
                    text: 'Top Applications by Data'
                },
                legend: {
                    display: false,
                    position : 'right',
                    labels : {
                        fontColor : '#FFFFFF'
                    }
                }
            };
        };
        $scope.processTopAppUserChart = function(data) {
            $scope.labels['barTopAppUser'] = [];
            $scope.series['barTopAppUser'] =  { line : ['Data']};
            $scope.data['barTopAppUser'] = [];
            data.forEach(function (val) {
                $scope.labels['barTopAppUser'].push(val._id);
                $scope.data['barTopAppUser'].push(val.count);
            });
            $scope.datasetOverride['barTopAppUser'] = [{
                yAxisId: 'y-axis-1'
            }];
            $scope.options['barTopAppUser'] = {
                title : {
                    display : false,
                    text : 'Top Applications by User'
                },
                scales : {
                    yAxes : [{
                        id : 'y-axis-1',
                        display: true,
                        title : 'Count',
                        position:'left'
                    }]
                },
                tooltips: {
                    enabled: true,
                    mode: 'single',
                    callbacks: {
                        label: function(tooltipItems, data) {
                            return tooltipItems.yLabel;
                        }
                    }
                }
            };
        };
        $scope.processTopTimeOfAccessChart = function(data) {
            $scope.labels['lineTopTimeOfAccessChart'] = [];
            $scope.series['lineTopTimeOfAccessChart'] =  { line : ['Data']};
            $scope.data['lineTopTimeOfAccessChart'] = [];
            data.forEach(function (val) {
                $scope.labels['lineTopTimeOfAccessChart'].push(val.name);
                $scope.data['lineTopTimeOfAccessChart'].push(val.count);
            });
            $scope.datasetOverride['lineTopTimeOfAccessChart'] = [{
                yAxisId: 'y-axis-1'
            }];
            $scope.options['lineTopTimeOfAccessChart'] = {
                title : {
                    display : false,
                    text : 'Top Time of Access'
                },
                scales : {
                    yAxes : [{
                        id : 'y-axis-1',
                        display: true,
                        title : 'Count',
                        position:'left'
                    }]
                },
                tooltips: {
                    enabled: true,
                    mode: 'single',
                    callbacks: {
                        label: function(tooltipItems, data) {
                            return tooltipItems.yLabel;
                        }
                    }
                }
            };
        };
        $scope.loadPerformanceReport = function(start, end) {
            AccessLogService.get({limit : 5, auth : 1, page : 1}, function(data) {
                if(data.success) {
                    $scope.latestFailedLogins = data.data;
                }
            });
            PolicyLogService.get({limit : 5, page : 1}, function(data) {
                if(data.success) {
                    $scope.latestViolatedPolicies = data.data;
                }
            });
            $scope.loadingPerformance = true;
            DashboardService.get({action : 'performanceReport', start : start, end : end}, function(data) {
                $scope.loadingPerformance = false;
                if(data.success) {
                    $scope.performanceData = data.data;
                    $scope.processPerformanceChart($scope.performanceModuleFilter);
                }
            }, function(err) {
                $scope.loadingPerformance = false;
                console.log(err);
            });
            DashboardService.get({action : 'getTopAppByData', start : start, end : end}, function(data) {
                if(data.success) {
                    $scope.processTopAppDataChart(data.data);
					//alert (JSON.stringify($scope.processTopAppDataChart));
					console.log($scope.processTopAppDataChart);
                }
            });
            DashboardService.get({action : 'getTopAppByUsers', start : start, end : end}, function(data) {
                if(data.success) {
                    $scope.processTopAppUserChart(data.data);
                }
            });
            DashboardService.get({action : 'getTopDevices', start : start, end : end}, function(data) {
                if(data.success) {
                    $scope.topDeviceList = data.data;
                } else {
                    $scope.topDeviceList = [];
                }
            });
            DashboardService.get({action : 'getTopLocations', start : start, end : end}, function(data) {
                if(data.success) {
                    $scope.topLocationList = data.data;
                } else {
                    $scope.topLocationList = [];
                }
            });
            DashboardService.get({action : 'getTopTimeOfAccess', start : start, end : end}, function(data) {
                if(data.success) {
                    $scope.processTopTimeOfAccessChart(data.data);
                }
            });
        };
        Chart.defaults.global.defaultFontColor = '#FFF';
        $scope.processPerformanceChart = function(filter) {
            if($scope.performanceData) {
                // console.log($scope.performanceData);
                $scope.labels['linePerformance'] = [];
                $scope.series['linePerformance'] =  { line : ['Data']};
                $scope.data['linePerformance'] = [[]];
                var modulesIds = [];
                $scope.performanceData.forEach(function(perfData) {
                    if(perfData.module == filter.toLowerCase()) {
                        var lineIndex = modulesIds.indexOf(perfData.moduleId);
                        if(lineIndex == -1) {
                            modulesIds.push(perfData.moduleId);
                            lineIndex = modulesIds.indexOf(perfData.moduleId);
                            $scope.data['linePerformance'][lineIndex] = [];
                        }
                        $scope.labels['linePerformance'].push(perfData.tag);
                        $scope.data['linePerformance'][lineIndex].push(parseFloat((perfData.avg/1000000).toFixed(4)));
                    }
                });
                //console.log($scope.data['linePerformance'][0], $scope.labels['linePerformance']);
                $scope.datasetOverride['linePerformance'] = [{
                    yAxisId: 'y-axis-1'
                }];
                $scope.options['linePerformance'] = {
                    title : {
                        display : false,
                        text : 'Avg. module performance'
                    },
                    scales : {
                        yAxes : [{
                            id : 'y-axis-1',
                            display: true,
                            title : 'Data',
                            position:'left'
                        }]
                    },
                    tooltips: {
                        enabled: true,
                        mode: 'single',
                        callbacks: {
                            label: function(tooltipItems, data) {
                                return tooltipItems.yLabel + ' ms';
                            }
                        }
                    }
                };
            }
        };
        $scope.processDataForCharts = function(data) {
            if(data.activeUsers.length > 0) {
                $scope.labels['pie'] = [];
                $scope.data['pie'] = [];
                data.activeUsers.forEach(function(val) {
                    if(val.key) {
                        $scope.labels['pie'].push(val.key);
                        $scope.data['pie'].push(parseInt(val.value));
                    }
                });
                $scope.options['pie'] = {
                    title: {
                        display: false,
                        text: 'Most active users'
                    },
                    legend: {
                        display: true,
                        position : 'right',
                        labels : {
                            fontColor : '#FFFFFF'
                        }
                    }
                };
            }
        };
        $scope.doFilter = function(filters) {
            $scope.dashboardService = new DashboardService();
            if(filters.daterange.toLowerCase() == 'past week') {
                $scope.dashboardService.data = {
                    start : moment().subtract(6, 'days').valueOf(),
                    end : moment().valueOf()
                };
            } else {
                $scope.dashboardService.data = {
                    start : moment().subtract(29, 'days').valueOf(),
                    end : moment().valueOf()
                };
            }
            $scope.loadPerformanceReport($scope.dashboardService.data.start, $scope.dashboardService.data.end);
            $scope.dashboardService.$save(function(success) {
                console.log(success);
                if(success.success) {
                    $scope.processDataForCharts(angular.copy(success.data));
                    $scope.stats = angular.copy(success.data);
                    delete $scope.stats.activeUsers;
                    delete $scope.stats.dateTransferred;
                }
            }, function(err) {
                console.log(err);
                alert('Unable to get data from server');
            })
        };
        $scope.loadingHeartBeat = false;
        $scope.serverStatusHeartBeat = {
            'PE'    : false,
            'DSWC'  : false,
            'DSS'   : false,
            'AS'    : false
        };
        $scope.getHeartBeatData = function() {
            $scope.loadingHeartBeat = true;
            DashboardService.get({}, function(data) {
                $scope.loadingHeartBeat = false;
                if(data.response.data && data.response.data.length > 0) {
                    data.response.data.forEach(function(val) {
                        $scope.serverStatusHeartBeat[val.serverName] = true;
                    });
                } else {
                    $scope.serverStatusHeartBeat = {
                        'PE'    : false,
                        'DSWC'  : false,
                        'DSS'   : false,
                        'AS'    : false
                    };
                }
            }, function(err) {
                $scope.loadingHeartBeat = false;
            });
        };
        $scope.getHeartBeatData();
        $scope.heartBeatInterval = $interval($scope.getHeartBeatData, 60000);
        $scope.$on('$destroy', function() {
            $interval.cancel($scope.heartBeatInterval);
        });
        $scope.doFilter($scope.filterData);
        $scope.onClick = function (points, evt) {
            console.log(points, evt);
        };
    })
    .controller('DeploymentController', function($scope, $rootScope, $http, $interval) {
        $rootScope.menuItem = 'deployment';
        $rootScope.pageTitle = 'Deployment Controller';
        if(!'isDeployed' in $rootScope) {
            $rootScope.isDeployed = false;
        }
        //$scope.activeTab = 'manual';
        $scope.serverIp = [];
        $scope.serverUsername = [];
        $scope.serverFile = [];
        $scope.deploying = false;
        $scope.submitDeploymentInfo = function(formData) {
            $scope.deploying = true;
            var uploadData = new FormData();

            uploadData.append('serverIpDSWFC', formData.serverIp[0]);
            uploadData.append('serverIpDSS', formData.serverIp[1]);
            uploadData.append('serverIpPE', formData.serverIp[2]);

            uploadData.append('serverUsernameDSWFC', formData.serverUsername[0]);
            uploadData.append('serverUsernameDSS', formData.serverUsername[1]);
            uploadData.append('serverUsernamePE', formData.serverUsername[2]);

            uploadData.append('serverFileDSWFC', formData.serverFile[0]);
            uploadData.append('serverFileDSS', formData.serverFile[1]);
            uploadData.append('serverFilePE', formData.serverFile[2]);

            $http({
                url : '/api/deployment',
                method : 'POST',
                data : uploadData,
                responseType: 'json',
                cache : false,
                headers: {
                    'Content-Type': undefined
                }
            }).then(function(success) {
                $scope.deploying = false;
                success = success.data;
                if(success.success) {
                    $rootScope.isDeployed = true;
                    $rootScope.deploymentUrl = angular.copy(formData.serverIp);
                    $rootScope.deploymentUsername = angular.copy(formData.serverUsername);
                    $rootScope.deploymentFile = [
                        success.DSWFCFile,
                        success.DSSFile,
                        success.PEFile
                    ];
                    if(!success.PE) {
                        $('.PE.label').removeClass('background-color--mint').addClass('background-color--secondary').text('Not Deployed');
                    } else {
                        $('.PE.label').removeClass('background-color--secondary').addClass('background-color--mint').text('Deployed');
                    }
                    if(!success.DSS) {
                        $('.DSS.label').removeClass('background-color--mint').addClass('background-color--secondary').text('Not Deployed');
                    } else {
                        $('.DSS.label').removeClass('background-color--secondary').addClass('background-color--mint').text('Deployed');
                    }
                    if(!success.DSWFC) {
                        $('.DSWFC.label').removeClass('background-color--mint').addClass('background-color--secondary').text('Not Deployed');
                    } else {
                        $('.DSWFC.label').removeClass('background-color--secondary').addClass('background-color--mint').text('Deployed');
                    }
                    $scope.serverIp = [];
                    $scope.serverUsername = [];
                    $scope.serverFile = [];
                    angular.element(document.getElementsByName('controllerUIForm'))[0].reset();
                    $scope.startStatusMonitor();
                }
            }, function(error) {
                console.log(error);
                $scope.deploying = false;
                alert('Something went wrong. Deploy error. Please try again later');
                location.reload();
            });
        };
        $scope.deleteDeployment = function() {
            var uploadData = {
                serverIpDSWFC: $rootScope.deploymentUrl[0],
                serverIpDSS: $rootScope.deploymentUrl[1],
                serverIpPE: $rootScope.deploymentUrl[2],

                serverUsernameDSWFC: $rootScope.deploymentUsername[0],
                serverUsernameDSS: $rootScope.deploymentUsername[1],
                serverUsernamePE: $rootScope.deploymentUsername[2],

                serverFileDSWFC: $rootScope.deploymentFile[0],
                serverFileDSS: $rootScope.deploymentFile[1],
                serverFilePE: $rootScope.deploymentFile[2]
            };

            $http({
                url : '/api/deployment/delete',
                method : 'PUT',
                data : uploadData,
                responseType: 'json',
                cache : false,headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function(success) {
                console.log(success);
                $rootScope.isDeployed = false;
            }, function(err) {
                console.log(err);
            });
        };
        $scope.healthCheck = function() {
            $httpConfig = {timeout: 5000, responseType  : 'json'};
            // For DSWFC service
            $http.get('http://' + $rootScope.deploymentUrl[0] + ':8080/health', $httpConfig).then(function(success) {
                //console.log('DSWFC', success);
                if(success.data.status == 'UP') {
                    $('.DSWFC.label').removeClass('background-color--secondary').addClass('background-color--mint').text('Working');
                } else {
                    $('.DSWFC.label').removeClass('background-color--mint').addClass('background-color--secondary').text('Not Working');
                }
            }, function(error) {
                //console.error('DSWFC', error);
                $('.DSWFC.label').removeClass('background-color--mint').addClass('background-color--secondary').text('Not Working');
            });
            // For DSS service
            $http.get('http://' + $rootScope.deploymentUrl[1] + ':8070/health', $httpConfig).then(function(success) {
                //console.log('DSS', success);
                if(success.data.status == 'UP') {
                    $('.DSS.label').removeClass('background-color--secondary').addClass('background-color--mint').text('Working');
                } else {
                    $('.DSS.label').removeClass('background-color--mint').addClass('background-color--secondary').text('Not Working');
                }
            }, function(error) {
                //console.error('DSS', error);
                $('.DSS.label').removeClass('background-color--mint').addClass('background-color--secondary').text('Not Working');
            });
            // For PE service
            $http.get('http://' + $rootScope.deploymentUrl[2] + ':8060/health', $httpConfig).then(function(success) {
                //console.log('PE', success);
                if(success.data.status == 'UP') {
                    $('.PE.label').removeClass('background-color--secondary').addClass('background-color--mint').text('Working');
                } else {
                    $('.PE.label').removeClass('background-color--mint').addClass('background-color--secondary').text('Not Working');
                }
            }, function(error) {
                //console.error('PE', error);
                $('.PE.label').removeClass('background-color--mint').addClass('background-color--secondary').text('Not Working');
            });
        };
        $scope.startStatusMonitor = function() {
            $scope.statusTimer = $interval($scope.healthCheck, 60000);
        };
        if($rootScope.isDeployed) {
            $scope.healthCheck();
            $scope.startStatusMonitor();
        }
        $scope.$on('$destroy', function() {
            if($scope.statusTimer) {
                $interval.cancel($scope.statusTimer);
            }
        })
    });