/**
 * Created by Sanjay Yadav on Oct 08 2017.
 */
TopengUI
// console.log(JSON.stringify(TopengUI));
    .controller('ServicesManagementController', function($scope, $rootScope, ServiceManagementService,StandardService, AttributeService, $state, $location, $anchorScroll, $timeout) {
        $rootScope.menuItem = 'services-management';
        $rootScope.pageTitle = 'Services Management';
        //console.log(JSON.stringify(ServiceManagementService));
        
        $scope.loadAttributes = function(pageNo, type) {
            // alert('loading...')
            $scope.showProgress = true;
            ServiceManagementService.get({listBy : type, page : 1}, function(success) {
                $scope.showProgress = false;
                // console.log(JSON.stringify(success));
                if(success.success) {
                    if(success.data.length == 0 && $scope.page > 1) {
                        $scope.page -= 1;
                    }
                    $scope.noNext = success.data.length < 10;
                    $scope.attributeList = success.data;
					//alert (JSON.stringify($scope.attributeList));
                }
            }, function(err) {
                $scope.showProgress = false;
                //console.log(err);
            });
        };
        $scope.callNextPage = function(currentPage, type) {
            $scope.page += currentPage;
            if($scope.page > 1) {
                $scope.noPrevious = false;
            } else if($scope.page == 1) {
                $scope.noPrevious = true;
            }
            if($scope.page > 0) {
                $scope.loadAttributes($scope.page, type);
            } else {
                $scope.page = 1;
            }
        };
        $scope.callNextPage(0, 'all');
        $scope.editAttribute = function(attribute) {
            //console.log(attribute);
            $scope.showProgress = true;
            $location.hash('header');
            $anchorScroll();
            ServiceManagementService.get({attributeId : attribute.id}, function(success) {
                $scope.showProgress = false;
                //console.log(success);
                if(success.success) {
                    attribute.aliases.splice(0, 1);
                    $scope.editId = attribute.id;
                    $scope.attributeData = {
                        attributeName : attribute.name,
						iconUrl : attribute.iconUrl,
                        listAliases : attribute.aliases,
                        finalStandards : [],
                        standards : {}
                    };
                    attribute.standards.forEach(function(stdId) {
                        $scope.attributeData.standards[stdId] = true;
                    });
                    $scope.standardList.forEach(function(stds) {
                        if($scope.attributeData.standards[stds.id]) {
                            $scope.attributeData.finalStandards.push(stds);
                        }
                    });
                    $timeout(function() {
                        angular.element(document.getElementsByClassName('is-invalid')).addClass('is-dirty').removeClass('is-invalid');
                    }, 200);
                    $scope.toggleAddBox('update');
                } else {
                    $scope.editId = undefined;
                }
            }, function(err) {
                $scope.showProgress = false;
                $scope.editId = undefined;
                //console.log(err);
            });
        };
        $scope.deleteAttribute = function(id, event) {
            var deleteElement = angular.element(event.target).parent().parent().parent();
            if(confirm('Are you sure you want to delete attribute?')) {
                $scope.showProgress = true;
                var attrService = new ServiceManagementService();
                attrService.attributeId = id;
                attrService.$delete(function(data) {
                    $scope.showProgress = false;
                    //console.log(data);
                    if(data.success) {
                        deleteElement.remove();
                    }
                }, function(err) {
                    $scope.showProgress = false;
                    //console.log(err);
                    alert('Unable to get data from server');
                });
            }
        };
        StandardService.get({}, function(data) {
            if(data.success && data.data.standardList.length > 0) {
                $scope.standardList = data.data.standardList;
                $scope.attributeData.finalStandards = [];
                if($scope.attributeData.standards.length > 0) {
                    $scope.standardList.forEach(function(stds) {
                        if($scope.attributeData.standards[stds.id]) {
                            $scope.attributeData.finalStandards.push(stds);
                        }
                    });
                }
            }
        });

        // Add/Update Wizard
        $scope.data = {};
        $scope.data.currentStep = 1;
        $scope.addAttributeToggle = false;
        $rootScope.overFlow = false;
        $scope.toggleAddBox = function(target) {
            $scope.data.currentStep = 1;
            if(!target) {
                $scope.editId = undefined;
                $scope.attributeData = {
                    listAliases : [],
                    finalStandards : [],
                    standards : {}
                };
                $timeout(function() {
                    angular.forEach(angular.element(document.getElementsByClassName('ng-invalid')), function(el) {
                        if(el.tagName.toLowerCase() != 'form') {
                            angular.element(el).parent().addClass('is-invalid').removeClass('is-dirty');
                        }
                    });
                }, 200);
            } else {

            }
            $rootScope.overFlow = !$scope.addAttributeToggle;
            $scope.addAttributeToggle = !$scope.addAttributeToggle;
        };
        $scope.attributeData = {
            listAliases : [],
            finalStandards : [],
            standards : {}
        };
        $scope.addAlias = function(name) {
            if(name && $scope.attributeData.listAliases.indexOf(name) == -1) {
                $scope.attributeData.listAliases.splice(0, 0, name);
                $scope.data.newAlias = undefined;
            }
        };
        $scope.removeAlias = function(name) {
            if((index = $scope.attributeData.listAliases.indexOf(name)) != -1) {
                $scope.attributeData.listAliases.splice(index, 1);
            }
        };
        $scope.addRemoveItem = function (checked, value) {
            if (checked) {
                $scope.attributeData.finalStandards.push(value);
            }
            else {
                var index = $scope.attributeData.finalStandards.indexOf(value);
                $scope.attributeData.finalStandards.splice(index, 1);
            }
        };
        $scope.submitCreateAttribute = function() {
            var postData = {
                serviceName : $scope.attributeData.attributeName,
                iconUrl : $scope.attributeData.iconUrl,
                acceptedDomains : $scope.attributeData.listAliases,
                
            };
            //console.log(postData);return;
			alert (JSON.stringify(postData));
            var attrService = new ServiceManagementService();
            console.log(attrService);
            attrService.data = postData;
            $scope.savingAttribute = true;
            console.log(attrService.data);
            if($scope.editId) {
                postData.id = $scope.editId;
                attrService.attributeId = $scope.editId;
                attrService.$update(function(success) {
                    $scope.savingAttribute = false;
                    //console.log(success);
                    if(success.success) {
                        $scope.toggleAddBox();
                        $scope.attributeData.detect_regex = undefined;
                        $timeout(function() {
                            $scope.callNextPage(0, 'all');
                        }, 200);
                    }
                }, function(err) {
                    $scope.savingAttribute = false;
                    //console.log(err);
                });
            } else {
                attrService.$save(function(success) {
                    $scope.savingAttribute = false;
                    //console.log(success);
                    if(success.success) {
                        $scope.toggleAddBox();
                        $timeout(function() {
                            $scope.callNextPage(0, 'all');
                        }, 200);
                    }
                }, function(err) {
                    $scope.savingAttribute = false;
                    //console.log(err);
                });
            }
        }
    })