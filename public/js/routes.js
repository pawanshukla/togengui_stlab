TopengUI.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('dashboard', {
            url : '/dashboard',
            controller: 'DashboardController',
            templateUrl: 'templates/dashboard.html'
        })
        .state('deployment', {
            url : '/deployment',
            controller: 'DeploymentController',
            templateUrl: 'templates/deployment.html'
        })
        .state('ipwhitelist', {
            url : '/ipwhitelist',
            controller : 'IPWhiteListController',
            templateUrl : 'templates/ip-whitelisting.html'
        })
        .state('users', {
            url : '/users',
            cache : false,
            controller: 'UserController',
            templateUrl: 'templates/listusers.html'
        })
        .state('users.add', {
            parent : 'users',
            url : '/add',
            controller: 'AddUserController',
            templateUrl: 'templates/adduser.html'
        })
        .state('users.edit', {
            parent : 'users',
            url : '/edit/:userid',
            controller: 'AddUserController',
            templateUrl: 'templates/adduser.html'
        })
        .state('groups', {
            url : '/groups',
            cache : false,
            controller : 'GroupsController',
            templateUrl: 'templates/listgroups.html'
        })
        .state('standards', {
            url : '/standards',
            cache : false,
            controller : 'StandardsController',
            templateUrl: 'templates/liststandards.html'
        })
        .state('policies', {
            url : '/policies',
            cache : false,
            controller : 'PolicyController',
            templateUrl: 'templates/listpolicies.html'
        })
        .state('policies.edit', {
            url : '/edit/:policyid',
            controller: 'AddPolicyController',
            templateUrl: 'templates/addpolicy.html'
        })
        .state('profile', {
            url : '/profile',
            controller : 'ProfileController',
            templateUrl: 'templates/profile.html'
        })
        .state('configurations', {
            url : '/configurations',
            templateUrl: 'templates/config.html',
            controller : 'ConfigController'
        })
        .state('audit', {
            url : '/audit',
            templateUrl: 'templates/audit.html',
            controller : 'AuditController'
        })
        .state('accesslog', {
            url : '/accesslog',
            templateUrl: 'templates/accesslog.html',
            controller : 'AccessLogController'
        })
        .state('policylog', {
            url : '/policylog',
            templateUrl: 'templates/policylog.html',
            controller : 'PolicyLogController'
        })
        .state('attributes', {
            url : '/attributes',
            cache : false,
            templateUrl: 'templates/listattributes.html',
            controller : 'AttributesController'
        })
        .state('services_management', {
            url : '/service_management',
            cache : false,
            templateUrl: 'templates/services_management.html',
            controller : 'ServicesManagementController'
        })
        .state('devices_registry', {
            url : '/device_registry',
            cache : false,
            templateUrl: 'templates/devices_registry.html',
            controller : 'DevicesRegistryController'
        })
        .state('gateways_management', {
            url : '/gateway_management',
            cache : false,
            templateUrl: 'templates/gateways_management.html',
            controller : 'GatewaysManagementController'
        })

        .state('sdk_management', {
            url : '/sdk_management',
            cache : false,
            templateUrl: 'templates/list_sdks.html',
            controller : 'SDKController'
        })
        .state('importldap', {
            url : '/importldap',
            cache : false,
            templateUrl: 'templates/import_from_ldap.html',
            controller : 'ImportLdapController'
        })
		.state('onedrive', {
            url : '/onedrive',
            templateUrl: 'templates/onedrive.html',
            controller : 'OnedriveController'
        })
        // .state('onboard-devices', {
        //     url : '/onboard-devices',
        //     cache : false,
        //     templateUrl: 'templates/onboard_devices.html',
        //     controller : 'OnboardDevicesPerimeterController'
        // })
        // .state('perimeter-user', {
        //     url : '/perimeter-user',
        //     cache : false,
        //     templateUrl: 'templates/perimeter_user.html',
        //     controller : 'PerimeterUserController'
        // })
        // .state('perimeter-gateways', {
        //     url : '/perimeter-gateways',
        //     cache : false,
        //     templateUrl: 'templates/perimeter_gateway.html',
        //     controller : 'PerimeterGatewaysController'
        // })
        // .state('perimeter-controller', {
        //     url : '/perimeter-controller',
        //     cache : false,
        //     templateUrl: 'templates/perimeter_controller.html',
        //     controller : 'PerimeterController'
        // })
        // .state('perimeter-accesslog', {
        //     url : '/perimeter-accesslog',
        //     cache : false,
        //     templateUrl: 'templates/perimeter-accesslog.html',
        //     controller : 'PerimeterLogController'
        // })
        .state('logout', {
            url : '/logout',
            controller : function($scope, $state, Logout) {
                Logout.doLogout();
            }
        });
    //$urlRouterProvider.otherwise('/login');
    $urlRouterProvider.otherwise('/dashboard');
});