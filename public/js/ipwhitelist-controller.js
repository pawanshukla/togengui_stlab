/**
 * Created by AnkurR on Jan 010 10/01/2017.
 */
TopengUI
    .controller('IPWhiteListController', function($scope, $rootScope, IpWhiteListService) {
        $rootScope.menuItem = 'ipwhitelist';
        $rootScope.controllerSubMenu = true;
        $rootScope.pageTitle = 'IP Whitelisting';
        $rootScope.controllerSubMenu = true;
        $scope.ipListing = [];
        $scope.listIps = function(module) {
            if(module) {
                $scope.savingData[module] = true;
            } else {
                $scope.savingData = {
                    dswc : true,
                    dss : true,
                    pe : true
                };
            }
            IpWhiteListService.get({}, function(success) {
                if(module) {
                    $scope.savingData[module] = false;
                } else {
                    $scope.savingData = {
                        dswc : false,
                        dss : false,
                        pe : false
                    };
                }
                if(success.success) {
                    $scope.ipListing = success.data;
                }
                console.log(success);
            }, function(err) {
                if(module) {
                    $scope.savingData[module] = false;
                } else {
                    $scope.savingData = {
                        dswc : false,
                        dss : false,
                        pe : false
                    };
                }
                console.log(err);
            });
        };
        $scope.listIps();
        $scope.addWhiteIp = function(module, Ip) {
            if(Ip) {
                $scope.savingData[module] = true;
                var ipWhitelist = new IpWhiteListService();
                ipWhitelist.data = {
                    module : module,
                    moduleId : '1',
                    subnet : Ip,
                    action : 'add'
                };
                ipWhitelist.$save(function(success) {
                    $scope.savingData[module] = false;
                    console.log(success);
                    if(success.success) {
                        $scope[module + 'Ip']= null;
                        $scope.listIps(module);
                    } else {
                        alert(success.msg);
                    }
                }, function(err) {
                    $scope.savingData[module] = false;
                    console.log(err);
                });
            }
        };
        $scope.deleteSubnet = function(module, subnet) {
            if(confirm('Are you sure you want to delete this subnet?')) {
                $scope.savingData[module] = true;
                var ipWhitelist = new IpWhiteListService();
                ipWhitelist.data = {
                    module : module,
                    moduleId : '1',
                    subnet : subnet,
                    action : 'delete'
                };
                ipWhitelist.$save(function(success) {
                    $scope.savingData[module] = false;
                    console.log(success);
                    if(success.success) {
                        $scope.listIps(module);
                    }
                }, function(err) {
                    $scope.savingData[module] = false;
                    console.log(err);
                });
            }
        };
    });
