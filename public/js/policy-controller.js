TopengUI
    .controller('PolicyController', function($scope, $rootScope, PolicyService, CountryList, GroupService, UserService, StandardService, $state, AttributeService, $timeout, countryWithRegions, filterFilter, SDKService, $interval) {
        $rootScope.menuItem = 'policy';
        $rootScope.pageTitle = 'Policy Management';
        countryWithRegions.then(function(data) {
            $scope.countryWithRegions = data;
        });
        $scope.CountryList = CountryList;
        AttributeService.get({listBy:'all'}, function(attrs) {
            if(attrs.success) {
                $scope.attrList = {};
                $scope.allAttributeList = attrs.data;
                attrs.data.forEach(function(attr) {
                    $scope.attrList[attr.id] = attr.name;
                });
            }
        });
        $scope.loadPolicies = function(page) {
            $scope.showProgress = true;
            PolicyService.get({page : page}, function(success) {
                $scope.showProgress = false;
                //console.log(success);
                if(success.success) {
                    $scope.policylist = {data : success.data.phalicyList };
                }
            }, function(error) {
                $scope.showProgress = false;
                console.error(error);
                alert('Unable to get data from server');
            });
        };
        $scope.getResponseAction = function(id) {
            switch (id) {
                case 1:
                    return 'Encrypt';
                    break;
                case 2:
                    return 'Tokenize';
                    break;
                case 3:
                    return 'DataMask';
                    break;
            }
        };
        $scope.loadPolicies(1);
        $scope.callNextPage = function(currentPage) {
            $scope.loadPolicies(currentPage);
        };
        $scope.deletePolicy = function(policyId, event) {
            var deleteElement = angular.element(event.target).parent().parent().parent();
            if(confirm('Are you sure you want to delete policy?')) {
                $scope.policyService = new PolicyService();
                $scope.policyService.policyId = policyId;
                $scope.policyService.$delete(function(success) {
                    deleteElement.remove();
                    if(success.success) {
                    }
                }, function(error) {
                    alert('Unable to get data from server');
                });
            }
        };

        // Add policy wizard start
        $scope.data = {};
        $scope.data.currentStep = 1;
        $scope.addPolicyToggle = false;
        $rootScope.overFlow = false;
        $scope.loaderFlags = 0;
        GroupService.get({}, function (data) {
            $scope.loaderFlags++;
            if (data.success) {
                $scope.groupList = data.data;
            }
        }, function (err) {
            $scope.loaderFlags++;
            console.log(err);
        });
        UserService.get({}, function(data) {
            $scope.loaderFlags++;
            if(data.success) {
                $scope.userList = data.data;
            }
        }, function(err) {
            $scope.loaderFlags++;
            console.log(err);
        });
        StandardService.get({}, function(data) {
            $scope.loaderFlags++;
            if(data.success) {
                $scope.standardList = data.data.standardList;
                if($scope.standardList.length > 0) {
                    $scope.standardChanged($scope.standardList[0]);
                }
            }
        }, function(err) {
            $scope.loaderFlags++;
            console.log(err);
        });
        SDKService.get({}, function(data) {
            $scope.loaderFlags++;
            if(data.success) {
                $scope.listApps = data.data;
            }
        }, function(err) {
            $scope.loaderFlags++;
        });
        $scope.toggleAddBox = function() {
            console.log('policy');
            $scope.data.currentStep = 1;
            $rootScope.overFlow2 = !$scope.addPolicyToggle;
            $scope.addPolicyToggle = !$scope.addPolicyToggle;
        };
        $scope.standardChanged = function(standard) {
            AttributeService.get({listBy : standard.id}, function(success) {
                console.log(success);
                if(success.success) {
                    $scope.stepActives.step6 = standard.name;
                    $scope.selectedData.step6.attributes = {};
                    $scope.selectedData.step6.finalAttributes = [];
                    $scope.listAttributes = success.data;
                }
            });
        };
        $scope.testCustomerRegex = function(testString, regex) {
            $scope.testResultString = '';
            pattern = new RegExp(regex);
            matches = pattern.exec(testString);
            if(matches && matches.length > 1) {
                matches.splice(0, 1);
                matches.forEach(function(val) {
                    testString = testString.replace(val, Array(val.length).join('*'));
                });
            }
            $scope.testResultString = testString;
        };
        $scope.showTestToggle = false;
        $scope.listOs = ['Windows', 'Linux', 'IOS', 'Android', 'MacOS', 'Blackberry'];
        $scope.listBrowsers = ['Chrome', 'Firefox', 'IE', 'Edge', 'Safari', 'Opera'];
        // $scope.listApps = ['Salesforce', 'Dr. Cloud EMR'];
        $scope.listEncAlgo = [
            {id : 'AES256', value : 'AES 256'},
            {id : 'AES128', value : 'AES 128'},
            {id : 'TRIPLEDES', value : 'Triple DES'},
            {id : 'RSA2048', value : 'RSA 2048'}
        ];
        $scope.maskMethods = [
            {id : '(.*)....', value: 'Show Last 4'},
            {id : '....(.*)', value: 'Show First 4'},
            {id : 'custom', value: 'Custom'}
        ];
        $scope.addRemoveItem = function (checked, value, subject) {
            switch (subject) {
                case 'users':
                    if (checked) {
                        $scope.selectedData.step1.finalUsers.push(value);
                    }
                    else {
                        var index = $scope.selectedData.step1.finalUsers.indexOf(value);
                        $scope.selectedData.step1.finalUsers.splice(index, 1);
                    }
                    break;
                case 'groups':
                    if (checked) {
                        $scope.selectedData.step1.finalGroups.push(value);
                    }
                    else {
                        var index = $scope.selectedData.step1.finalGroups.indexOf(value);
                        $scope.selectedData.step1.finalGroups.splice(index, 1);
                    }
                    break;
                case 'oss':
                    if (checked) {
                        $scope.selectedData.step2.finalOs.push(value);
                    }
                    else {
                        var index = $scope.selectedData.step2.finalOs.indexOf(value);
                        $scope.selectedData.step2.finalOs.splice(index, 1);
                    }
                    break;
                case 'browsers':
                    if (checked) {
                        $scope.selectedData.step2.finalBrowsers.push(value);
                    }
                    else {
                        var index = $scope.selectedData.step2.finalBrowsers.indexOf(value);
                        $scope.selectedData.step2.finalBrowsers.splice(index, 1);
                    }
                    break;
                case 'countries':
                    if (checked) {
                        if($scope.selectedData.step3.finalCountries.indexOf(value) === -1) {
                            $scope.selectedData.step3.finalCountries.push(value);
                        }
                    } else {
                        var index = $scope.selectedData.step3.finalCountries.indexOf(value);
                        $scope.selectedData.step3.finalCountries.splice(index, 1);
                        if(arguments[3]) {
                            $scope.selectedData.step3.regions[arguments[3].region] = false;
                            $scope.addRemoveItem($scope.selectedData.step3.regions[arguments[3].region], arguments[3], 'regions', 1);
                        }
                    }
                    break;
                case 'regions':
                    if (checked) {
                        $scope.selectedData.step3.finalRegions.push(value);
                        filterFilter($scope.countryWithRegions, {region : value.region}).forEach(function(val) {
                            $scope.selectedData.step3.countries[val['alpha-2']] = true;
                            $scope.addRemoveItem($scope.selectedData.step3.countries[val['alpha-2']], val, 'countries');
                        });
                    }
                    else {
                        var index = $scope.selectedData.step3.finalRegions.indexOf(value);
                        $scope.selectedData.step3.finalRegions.splice(index, 1);
                        if(!arguments[3]) {
                            filterFilter($scope.countryWithRegions, {region : value.region}).forEach(function(val) {
                                $scope.selectedData.step3.countries[val['alpha-2']] = false;
                                $scope.addRemoveItem($scope.selectedData.step3.countries[val['alpha-2']], val, 'countries');
                            });
                        }
                    }
                    break;
                case 'apps':
                    if (checked) {
                        $scope.selectedData.step4.finalApps.push(value);
                    }
                    else {
                        var index = $scope.selectedData.step4.finalApps.indexOf(value);
                        $scope.selectedData.step4.finalApps.splice(index, 1);
                    }
                    break;
                case 'attribute':
                    if (checked) {
                        $scope.selectedData.step6.finalAttributes.push(value);
                    }
                    else {
                        var index = $scope.selectedData.step6.finalAttributes.indexOf(value);
                        $scope.selectedData.step6.finalAttributes.splice(index, 1);
                    }
                    break;
                default :
            }
        };
        $scope.selectedData = {
            step1 : {
                finalGroups : [],
                groups : {},
                finalUsers : [],
                users : {}
            },
            step2 : {
                finalOs : [],
                oss : {},
                finalBrowsers : [],
                browsers : {}
            },
            step3 : {
                countries : {},
                finalCountries : [],
                regions : {},
                finalRegions : []
            },
            step4 : {
                finalApps : [],
                apps : {}
            },
            step5 : {
                response : {}
            },
            step6 : {
                attributes : {},
                finalAttributes : []
            },
            step7 : {
                activity : ''
            },
            step8 : {
                policyName : '',
                policyDescription : ''
            }
        };
        $scope.stepActives = {
            step1 : 'group',
            step2 : 'os',
            step5 : '',
            step6 : ''
        };
        /*$scope.key_256 = aesjs.util.convertStringToBytes('ads65d4a65s4da8s7da98s7da456kkkd');
        $scope.doAESEncryption = function(string) {
            if(string) {
                if(string.length < 16) {
                    string += Array(17-string.length).join('0')
                } else if(string.length > 16 && string.length % 16 != 0) {
                    string += Array((16 - (string.length % 16)) + 1).join('0');
                }
                $scope.aesCBC = new aesjs.ModeOfOperation.cbc($scope.key_256);
                $scope.encOutput = $scope.aesCBC.encrypt(aesjs.util.convertStringToBytes(string));
                $scope.encOutputString = aesjs.util.convertBytesToString($scope.encOutput);
            } else {
                $scope.encOutputString = '';
            }
        };*/
        $scope.submitCreatePolicy = function() {
            if($scope.selectedData.step3.finalCountries.length > 0) {
                $scope.savingPolicy = true;
                var policyData = {
                    name : $scope.selectedData.step8.policyName,
                    description : $scope.selectedData.step8.policyDescription,
                    actionVariables: [
                        {name: "bits",      value: "256"},
                        {name: "type",      value: "PKI"}
                    ]
                };
                if($scope.selectedData.step3.finalCountries.length > 0) {
                    $scope.selectedData.step3.finalCountries.forEach(function(country) {
                        policyData.actionVariables.push({name : 'location', value : country['alpha-2']});
                    });
                }
                if($scope.selectedData.step4.finalApps.length > 0) {
                    $scope.selectedData.step4.finalApps.forEach(function(app) {
                        policyData.actionVariables.push({name: "app", value: app.appName});
                    });
                }
                if($scope.selectedData.step2.finalBrowsers.length > 0) {
                    $scope.selectedData.step2.finalBrowsers.forEach(function(browser) {
                        policyData.actionVariables.push({name: "browser", value: browser});
                    });
                }
                if($scope.selectedData.step2.finalOs.length > 0) {
                    $scope.selectedData.step2.finalOs.forEach(function(os) {
                        policyData.actionVariables.push({name: "os", value: os});
                    });
                }
                if($scope.selectedData.step5.response['enc']) {
                    policyData.defaultAction = 'ENC';
                    //policyData.default_encryption_key_size = $scope.selectedData.step5.response['enc'].id;
                } else if($scope.selectedData.step5.response['mask']) {
                    policyData.defaultAction = 'MASK';
                    policyData.actionVariables.push({
                        name : 'maskregex',
                        value : $scope.selectedData.step5.response['mask'].value == 'custom' ? $scope.selectedData.step5.response['mask'].regex : $scope.selectedData.step5.response['mask'].id
                    });
                } else if($scope.selectedData.step5.response['plain']) {
                    policyData.defaultAction = 'PLAIN';
                }
                policyData.dataAttributes = $scope.selectedData.step6.finalAttributes.map(function(attr) {
                    return attr.id;
                });
                $scope.policyService = new PolicyService();
                $scope.policyService.data = policyData;
                // console.log(policyData);return;
                $scope.policyService.$save(function(success) {
                    //console.log(success);
                    if(success.success) {
                        if(success.data) {
                            $scope.mapPolicyToUser(success.data);
                            $scope.mapPolicyToGroup(success.data);
                            $scope.savingPolicy = false;
                            $state.go('policies', null, { reload: true });
                        }
                    }
                }, function(error) {
                    console.error(error);
                    alert('Unable to get data from server');
                });
            }
        };
        $scope.mapPolicyToUser = function(policyId) {
            if($scope.selectedData.step1.finalUsers.length > 0) {
                $scope.selectedData.step1.finalUsers.forEach(function(user) {
                    UserService.get({userId : user.id}, function(userData) {
                        if (userData.success) {
                            userData = userData.data;
                            if(!userData.groups) {
                                userData.groups = [];
                            }
                            if(angular.isArray(userData.policies)) {
                                userData.policies.push(policyId);
                            } else {
                                userData.policies = [policyId];
                            }
                            $scope.userService = new UserService();
                            $scope.userService.data = userData;
                            $scope.userService.userId = user.id;
                            $scope.userService.$update(function (success) {
                                console.log(success);
                            });
                        }
                    });
                });
            }
        };
        $scope.mapPolicyToGroup = function(policyId) {
            if($scope.selectedData.step1.finalGroups.length > 0) {
                $scope.selectedData.step1.finalGroups.forEach(function(group) {
                    GroupService.get({groupId : group.id}, function(groupData) {
                        if (groupData.success) {
                            groupData = groupData.data;
                            if(angular.isArray(groupData.policies)) {
                                groupData.policies.push(policyId);
                            } else {
                                groupData.policies = [policyId];
                            }
                            $scope.groupService = new GroupService();
                            $scope.groupService.data = groupData;
                            $scope.groupService.groupId = group.id;
                            $scope.groupService.$update(function(success) {
                                console.log(success);
                            });
                        }
                    });
                });
            }
        };

        // Edit Policy
        $scope.toggleEditBox = function(source) {
            if(source == 'cancel') {
                $scope.policyData = {};
                $scope.editId = undefined;
                $scope.editPolicyToggle = false;
                $rootScope.overFlow2 = false;
            }
        };
        $scope.log = function() {
            console.log(arguments);
        };
        $scope.editPolicy = function(policyId) {
            if($scope.loaderFlags < 4) {
                return;
            }
            $rootScope.overFlow2 = true;
            $scope.showProgressEdit = true;
            PolicyService.get({policyId : policyId}, function(success) {
                $scope.showProgressEdit = false;
                if(success.success) {
                    $scope.editId = policyId;
                    $scope.policyData = success.data;
                    $scope.policyData.location = {};
                    $scope.policyData.app = {};
                    $scope.policyData.browser = {};
                    $scope.policyData.os = {};
                    var selectedAttributes = angular.copy($scope.policyData.dataAttributes);

                    $scope.policyData.dataAttributes = {};
                    $scope.allAttributeList.forEach(function(data) {
                        if(selectedAttributes.indexOf(data.id) != -1) {
                            $scope.policyData.dataAttributes[data.id] = true;
                        }
                    });
                    console.log($scope.policyData.dataAttributes);
                    success.data.actionVariables.forEach(function(val, key) {
                        if(val.name == "maskregex") {
                            if(val.value != '(.*)....' && val.value != '....(.*)') {
                                $scope.policyData.defaultMask = 'custom';
                            } else {
                                $scope.policyData.defaultMask = val.value;
                            }
                            $scope.policyData.maskregex = val.value;
                            return;
                        }
                        if(val.name == 'location') {
                            $scope.policyData.location[val.value] = true;
                            return;
                        }
                        if(val.name == 'app') {
                            $scope.policyData.app[val.value] = true;
                            return;
                        }
                        if(val.name == 'browser') {
                            $scope.policyData.browser[val.value] = true;
                            return;
                        }
                        if(val.name == 'os') {
                            $scope.policyData.os[val.value] = true;
                        }
                        $scope.editPolicyToggle = true;
                        $timeout(function() {
                            if($scope.policyData.defaultAction == 'MASK') {
                                angular.element(document.getElementsByClassName('custom-regex-input')).addClass('is-dirty').removeClass('is-invalid');
                            }
                            angular.element(document.getElementsByClassName('is-invalid')).addClass('is-dirty').removeClass('is-invalid');
                        }, 200);
                    });
                } else {
                    $scope.editId = undefined;
                }
            }, function(error) {
                $scope.editId = undefined;
                $scope.showProgressEdit = false;
                console.log(error);
                alert('Unable to get data from server');
                $state.go('policies', null, { reload: true });
            });
        };
        $scope.submitAddPolicy = function(formData) {
            formData.actionVariables = [
                { name : "bits",        value:"256"},
                { name : "type",        value:"PKI"}
            ];
            if(Object.keys(formData.location).length > 0) {
                angular.forEach(formData.location, function (country, key) {
                    if(country) {
                        formData.actionVariables.push({ name : "location", value:key});
                    }
                });
            }
            delete formData.location;
            if(Object.keys(formData.os).length > 0) {
                angular.forEach(formData.os, function(os, key) {
                    if(os) {
                        formData.actionVariables.push({ name : "os", value:key});
                    }
                });
            }
            delete formData.os;
            if(Object.keys(formData.browser).length > 0) {
                angular.forEach(formData.browser, function(browser, key) {
                    if(browser) {
                        formData.actionVariables.push({ name : "browser", value: key});
                    }
                });
            }
            delete formData.browser;
            if(Object.keys(formData.app).length > 0) {
                angular.forEach(formData.app, function(app, key) {
                    if(app) {
                        formData.actionVariables.push({ name : "app", value: key});
                    }
                });
            }
            delete formData.app;
            if(formData.defaultAction != 'MASK') {
                if(formData.defaultMask) {
                    delete formData.defaultMask;
                }
                if(formData.maskregex) {
                    delete formData.maskregex;
                }
            } else {
                if(formData.defaultMask == 'custom') {
                    delete formData.defaultMask;
                    formData.actionVariables.push({
                        name : "maskregex",
                        value : formData.maskregex
                    });
                } else {
                    formData.actionVariables.push({
                        name : "maskregex",
                        value : formData.defaultMask
                    });
                }
                delete formData.maskregex;
            }
            var selectedAttributes = formData.dataAttributes;
            formData.dataAttributes = [];
            angular.forEach(selectedAttributes, function(attr, key) {
                if(attr) {
                    formData.dataAttributes.push(key);
                }
             });
            //console.log(formData);return;
            $scope.showProgressEdit = true;
            formData.id = $scope.editId;
            $scope.policyService = new PolicyService();
            $scope.policyService.data = formData;
            $scope.policyService.policyId = $scope.editId;
            $scope.policyService.$update(function(success) {
                $scope.showProgressEdit = false;
                console.log(success);
                if(success.success) {
                    $scope.toggleEditBox('cancel');
                    $scope.loadPolicies(1);
                }
            }, function(error) {
                $scope.showProgressEdit = false;
                console.error(error);
                alert('Unable to get data from server');
            });
        }
    })