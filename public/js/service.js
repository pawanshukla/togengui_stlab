TopengUI
    .factory('Login', function($resource, API_URL) {
        return $resource(API_URL).get().$promise;
    })
    .factory('UserService', function($resource, API_URL) {
        return $resource(API_URL + 'user/:userId', { userId: '@userId' }, {
            update: {
                method: 'PUT'
            }
        });
    })
    .factory('GroupService', function($resource, API_URL) {
        return $resource(API_URL + 'group/:groupId', { groupId : '@groupId'}, {
            update: {
                method: 'PUT'
            }
        })
    })
    .factory('StandardService', function($resource, API_URL) {
        return $resource(API_URL + 'standard/:standardId', { standardId : '@standardId'}, {
            update: {
                method: 'PUT'
            }
        })
    })
    .factory('ServiceManagementService', function($resource, API_URL) {
        return $resource(API_URL + 'servicemanagement/:serviceId', { serviceId : '@serviceId'}, {
            update: {
                method: 'PUT'
            }
        })
    })
    .factory('PolicyService', function($resource, API_URL) {
        return $resource(API_URL + 'policy/:policyId', {policyId : '@policyId'}, {
            update: {
                method: 'PUT'
            }
        });
    })
    .factory('AttributeService', function($resource, API_URL) {
        return $resource(API_URL + 'attribute/:attributeId', {attributeId: '@attributeId'}, {
            update: {
                method: 'PUT'
            }
        });
    })
    .factory('SDKService', function($resource, API_URL) {
        return $resource(API_URL + 'sdk/:sdkId', {sdkId: '@sdkId'}, {
            update: {
                method: 'PUT'
            }
        });
    })
    .factory('OnBoardDevicesPerimeterService', function($resource, API_URL) {
        return $resource(API_URL + 'onboarddevices/:deviceId', {deviceId: '@deviceId'}, {
            update: {
                method: 'PUT'
            }
        });
    })
    .factory('PerimeterUsersService', function($resource, API_URL) {
        return $resource(API_URL + 'perimeterusers/:userId', {userId: '@userId'}, {
            update: {
                method: 'PUT'
            }
        });
    })
    .factory('PerimeterGatewaysService', function($resource, API_URL) {
        return $resource(API_URL + 'perimetergateways/:gatewayId', {gatewayId: '@gatewayId'}, {
            update: {
                method: 'PUT'
            }
        });
    })
    .factory('PermissionService', function($resource, API_URL) {
        return $resource(API_URL + 'perimeter/:permissionId', {permissionId: '@permissionId'}, {
            update: {
                method: 'PUT'
            }
        });
    })
    .factory('DataService', function($resource, API_URL) {
        return $resource(API_URL + 'importdata');
    })
    .factory('IpWhiteListService', function($resource, API_URL) {
        return $resource(API_URL + 'ipwhitelist/:ipId', {ipId: '@ipId'});
    })
    .factory('AuditService', function($resource, API_URL) {
        return $resource(API_URL + 'audit');
    })
    .factory('AccessLogService', function($resource, API_URL) {
        return $resource(API_URL + 'accesslog');
    })
    .factory('PolicyLogService', function($resource, API_URL) {
        return $resource(API_URL + 'policylog');
    })
    .factory('PerimeterAccessLogService', function($resource, API_URL) {
        return $resource(API_URL + 'perimeteraccesslog');
    })
    .factory('DashboardService', function($resource, API_URL) {
        return $resource(API_URL + 'dashboard/:action', {action : '@action'});
    })
	 .factory('OnedriveService', function($resource, API_URL) {
        return $resource(API_URL + 'onedrive');
    })
    .factory('CountryList', function() {
        return {
            'AF' : 'Afghanistan',
            'AL' : 'Albania',
            'DZ' : 'Algeria',
            'AS' : 'American Samoa',
            'AD' : 'Andorra',
            'AO' : 'Angola',
            'AI' : 'Anguilla',
            'AQ' : 'Antarctica',
            'AG' : 'Antigua and Barbuda',
            'AR' : 'Argentina',
            'AM' : 'Armenia',
            'AW' : 'Aruba',
            'AU' : 'Australia',
            'AT' : 'Austria',
            'AZ' : 'Azerbaijan',
            'BS' : 'Bahamas',
            'BH' : 'Bahrain',
            'BD' : 'Bangladesh',
            'BB' : 'Barbados',
            'BY' : 'Belarus',
            'BE' : 'Belgium',
            'BZ' : 'Belize',
            'BJ' : 'Benin',
            'BM' : 'Bermuda',
            'BT' : 'Bhutan',
            'BO' : 'Bolivia',
            'BA' : 'Bosnia and Herzegovina',
            'BW' : 'Botswana',
            'BV' : 'Bouvet Island',
            'BR' : 'Brazil',
            'IO' : 'British Indian Ocean Territory',
            'BN' : 'Brunei Darussalam',
            'BG' : 'Bulgaria',
            'BF' : 'Burkina Faso',
            'BI' : 'Burundi',
            'KH' : 'Cambodia',
            'CM' : 'Cameroon',
            'CA' : 'Canada',
            'CV' : 'Cape Verde',
            'KY' : 'Cayman Islands',
            'CF' : 'Central African Republic',
            'TD' : 'Chad',
            'CL' : 'Chile',
            'CN' : 'China',
            'CX' : 'Christmas Island',
            'CC' : 'Cocos (Keeling) Islands',
            'CO' : 'Colombia',
            'KM' : 'Comoros',
            'CG' : 'Congo',
            'CD' : 'Congo, the Democratic Republic of the',
            'CK' : 'Cook Islands',
            'CR' : 'Costa Rica',
            'HR' : 'Croatia',
            'CU' : 'Cuba',
            'CY' : 'Cyprus',
            'CZ' : 'Czech Republic',
            'DK' : 'Denmark',
            'DJ' : 'Djibouti',
            'DM' : 'Dominica',
            'DO' : 'Dominican Republic',
            'EC' : 'Ecuador',
            'EG' : 'Egypt',
            'SV' : 'El Salvador',
            'GQ' : 'Equatorial Guinea',
            'ER' : 'Eritrea',
            'EE' : 'Estonia',
            'ET' : 'Ethiopia',
            'FK' : 'Falkland Islands (Malvinas)',
            'FO' : 'Faroe Islands',
            'FJ' : 'Fiji',
            'FI' : 'Finland',
            'FR' : 'France',
            'GF' : 'French Guiana',
            'PF' : 'French Polynesia',
            'TF' : 'French Southern Territories',
            'GA' : 'Gabon',
            'GM' : 'Gambia',
            'GE' : 'Georgia',
            'DE' : 'Germany',
            'GH' : 'Ghana',
            'GI' : 'Gibraltar',
            'GR' : 'Greece',
            'GL' : 'Greenland',
            'GD' : 'Grenada',
            'GP' : 'Guadeloupe',
            'GU' : 'Guam',
            'GT' : 'Guatemala',
            'GN' : 'Guinea',
            'GW' : 'Guinea-Bissau',
            'GY' : 'Guyana',
            'HT' : 'Haiti',
            'HM' : 'Heard Island and Mcdonald Islands',
            'VA' : 'Holy See (Vatican City State)',
            'HN' : 'Honduras',
            'HK' : 'Hong Kong',
            'HU' : 'Hungary',
            'IS' : 'Iceland',
            'IN' : 'India',
            'ID' : 'Indonesia',
            'IR' : 'Iran, Islamic Republic of',
            'IQ' : 'Iraq',
            'IE' : 'Ireland',
            'IL' : 'Israel',
            'IT' : 'Italy',
            'JM' : 'Jamaica',
            'JP' : 'Japan',
            'JO' : 'Jordan',
            'KZ' : 'Kazakhstan',
            'KE' : 'Kenya',
            'KI' : 'Kiribati',
            'KR' : 'Korea, Republic of',
            'KW' : 'Kuwait',
            'KG' : 'Kyrgyzstan',
            'LV' : 'Latvia',
            'LB' : 'Lebanon',
            'LS' : 'Lesotho',
            'LR' : 'Liberia',
            'LY' : 'Libyan Arab Jamahiriya',
            'LI' : 'Liechtenstein',
            'LT' : 'Lithuania',
            'LU' : 'Luxembourg',
            'MO' : 'Macao',
            'MK' : 'Macedonia, the Former Yugoslav Republic of',
            'MG' : 'Madagascar',
            'MW' : 'Malawi',
            'MY' : 'Malaysia',
            'MV' : 'Maldives',
            'ML' : 'Mali',
            'MT' : 'Malta',
            'MH' : 'Marshall Islands',
            'MQ' : 'Martinique',
            'MR' : 'Mauritania',
            'MU' : 'Mauritius',
            'YT' : 'Mayotte',
            'MX' : 'Mexico',
            'FM' : 'Micronesia, Federated States of',
            'MD' : 'Moldova, Republic of',
            'MC' : 'Monaco',
            'MN' : 'Mongolia',
            'MS' : 'Montserrat',
            'MA' : 'Morocco',
            'MZ' : 'Mozambique',
            'MM' : 'Myanmar',
            'NA' : 'Namibia',
            'NR' : 'Nauru',
            'NP' : 'Nepal',
            'NL' : 'Netherlands',
            'AN' : 'Netherlands Antilles',
            'NC' : 'New Caledonia',
            'NZ' : 'New Zealand',
            'NI' : 'Nicaragua',
            'NE' : 'Niger',
            'NG' : 'Nigeria',
            'NU' : 'Niue',
            'NF' : 'Norfolk Island',
            'MP' : 'Northern Mariana Islands',
            'NO' : 'Norway',
            'OM' : 'Oman',
            'PK' : 'Pakistan',
            'PW' : 'Palau',
            'PS' : 'Palestinian Territory, Occupied',
            'PA' : 'Panama',
            'PG' : 'Papua New Guinea',
            'PY' : 'Paraguay',
            'PE' : 'Peru',
            'PH' : 'Philippines',
            'PN' : 'Pitcairn',
            'PL' : 'Poland',
            'PT' : 'Portugal',
            'PR' : 'Puerto Rico',
            'QA' : 'Qatar',
            'RE' : 'Reunion',
            'RO' : 'Romania',
            'RU' : 'Russian Federation',
            'RW' : 'Rwanda',
            'SH' : 'Saint Helena',
            'KN' : 'Saint Kitts and Nevis',
            'LC' : 'Saint Lucia',
            'PM' : 'Saint Pierre and Miquelon',
            'VC' : 'Saint Vincent and the Grenadines',
            'WS' : 'Samoa',
            'SM' : 'San Marino',
            'ST' : 'Sao Tome and Principe',
            'SA' : 'Saudi Arabia',
            'SN' : 'Senegal',
            'CS' : 'Serbia and Montenegro',
            'SC' : 'Seychelles',
            'SL' : 'Sierra Leone',
            'SG' : 'Singapore',
            'SK' : 'Slovakia',
            'SI' : 'Slovenia',
            'SB' : 'Solomon Islands',
            'SO' : 'Somalia',
            'ZA' : 'South Africa',
            'GS' : 'South Georgia and the South Sandwich Islands',
            'ES' : 'Spain',
            'LK' : 'Sri Lanka',
            'SD' : 'Sudan',
            'SR' : 'Suriname',
            'SJ' : 'Svalbard and Jan Mayen',
            'SZ' : 'Swaziland',
            'SE' : 'Sweden',
            'CH' : 'Switzerland',
            'SY' : 'Syrian Arab Republic',
            'TW' : 'Taiwan',
            'TJ' : 'Tajikistan',
            'TZ' : 'Tanzania, United Republic of',
            'TH' : 'Thailand',
            'TL' : 'Timor-Leste',
            'TG' : 'Togo',
            'TK' : 'Tokelau',
            'TO' : 'Tonga',
            'TT' : 'Trinidad and Tobago',
            'TN' : 'Tunisia',
            'TR' : 'Turkey',
            'TM' : 'Turkmenistan',
            'TC' : 'Turks and Caicos Islands',
            'TV' : 'Tuvalu',
            'UG' : 'Uganda',
            'UA' : 'Ukraine',
            'AE' : 'United Arab Emirates',
            'GB' : 'United Kingdom',
            'US' : 'United States',
            'UM' : 'United States Minor Outlying Islands',
            'UY' : 'Uruguay',
            'UZ' : 'Uzbekistan',
            'VU' : 'Vanuatu',
            'VE' : 'Venezuela',
            'VN' : 'Viet Nam',
            'VG' : 'Virgin Islands, British',
            'VI' : 'Virgin Islands, U.s.',
            'WF' : 'Wallis and Futuna',
            'EH' : 'Western Sahara',
            'YE' : 'Yemen',
            'ZM' : 'Zambia',
            'ZW' : 'Zimbabwe',
            'IM' : 'Isle of Man',
            'JE' : 'Jersey',
            'GG' : 'Guernsey',
            'LA' : 'Lao People\'s Democratic Republic',
            'KP' : 'Korea, Democratic People\'s Republic of'
        };
    })
    .factory('CountryListArray', function() {
        return [
            {iso2: 'AF', name : 'Afghanistan'},
            {iso2: 'AL', name : 'Albania'},
            {iso2: 'DZ', name : 'Algeria'},
            {iso2: 'AS', name : 'American Samoa'},
            {iso2: 'AD', name : 'Andorra'},
            {iso2: 'AO', name : 'Angola'},
            {iso2: 'AI', name : 'Anguilla'},
            {iso2: 'AQ', name : 'Antarctica'},
            {iso2: 'AG', name : 'Antigua and Barbuda'},
            {iso2: 'AR', name : 'Argentina'},
            {iso2: 'AM', name : 'Armenia'},
            {iso2: 'AW', name : 'Aruba'},
            {iso2: 'AU', name : 'Australia'},
            {iso2: 'AT', name : 'Austria'},
            {iso2: 'AZ', name : 'Azerbaijan'},
            {iso2: 'BS', name : 'Bahamas'},
            {iso2: 'BH', name : 'Bahrain'},
            {iso2: 'BD', name : 'Bangladesh'},
            {iso2: 'BB', name : 'Barbados'},
            {iso2: 'BY', name : 'Belarus'},
            {iso2: 'BE', name : 'Belgium'},
            {iso2: 'BZ', name : 'Belize'},
            {iso2: 'BJ', name : 'Benin'},
            {iso2: 'BM', name : 'Bermuda'},
            {iso2: 'BT', name : 'Bhutan'},
            {iso2: 'BO', name : 'Bolivia'},
            {iso2: 'BA', name : 'Bosnia and Herzegovina'},
            {iso2: 'BW', name : 'Botswana'},
            {iso2: 'BV', name : 'Bouvet Island'},
            {iso2: 'BR', name : 'Brazil'},
            {iso2: 'IO', name : 'British Indian Ocean Territory'},
            {iso2: 'BN', name : 'Brunei Darussalam'},
            {iso2: 'BG', name : 'Bulgaria'},
            {iso2: 'BF', name : 'Burkina Faso'},
            {iso2: 'BI', name : 'Burundi'},
            {iso2: 'KH', name : 'Cambodia'},
            {iso2: 'CM', name : 'Cameroon'},
            {iso2: 'CA', name : 'Canada'},
            {iso2: 'CV', name : 'Cape Verde'},
            {iso2: 'KY', name : 'Cayman Islands'},
            {iso2: 'CF', name : 'Central African Republic'},
            {iso2: 'TD', name : 'Chad'},
            {iso2: 'CL', name : 'Chile'},
            {iso2: 'CN', name : 'China'},
            {iso2: 'CX', name : 'Christmas Island'},
            {iso2: 'CC', name : 'Cocos (Keeling) Islands'},
            {iso2: 'CO', name : 'Colombia'},
            {iso2: 'KM', name : 'Comoros'},
            {iso2: 'CG', name : 'Congo'},
            {iso2: 'CD', name : 'Congo, the Democratic Republic of the'},
            {iso2: 'CK', name : 'Cook Islands'},
            {iso2: 'CR', name : 'Costa Rica'},
            {iso2: 'HR', name : 'Croatia'},
            {iso2: 'CU', name : 'Cuba'},
            {iso2: 'CY', name : 'Cyprus'},
            {iso2: 'CZ', name : 'Czech Republic'},
            {iso2: 'DK', name : 'Denmark'},
            {iso2: 'DJ', name : 'Djibouti'},
            {iso2: 'DM', name : 'Dominica'},
            {iso2: 'DO', name : 'Dominican Republic'},
            {iso2: 'EC', name : 'Ecuador'},
            {iso2: 'EG', name : 'Egypt'},
            {iso2: 'SV', name : 'El Salvador'},
            {iso2: 'GQ', name : 'Equatorial Guinea'},
            {iso2: 'ER', name : 'Eritrea'},
            {iso2: 'EE', name : 'Estonia'},
            {iso2: 'ET', name : 'Ethiopia'},
            {iso2: 'FK', name : 'Falkland Islands (Malvinas)'},
            {iso2: 'FO', name : 'Faroe Islands'},
            {iso2: 'FJ', name : 'Fiji'},
            {iso2: 'FI', name : 'Finland'},
            {iso2: 'FR', name : 'France'},
            {iso2: 'GF', name : 'French Guiana'},
            {iso2: 'PF', name : 'French Polynesia'},
            {iso2: 'TF', name : 'French Southern Territories'},
            {iso2: 'GA', name : 'Gabon'},
            {iso2: 'GM', name : 'Gambia'},
            {iso2: 'GE', name : 'Georgia'},
            {iso2: 'DE', name : 'Germany'},
            {iso2: 'GH', name : 'Ghana'},
            {iso2: 'GI', name : 'Gibraltar'},
            {iso2: 'GR', name : 'Greece'},
            {iso2: 'GL', name : 'Greenland'},
            {iso2: 'GD', name : 'Grenada'},
            {iso2: 'GP', name : 'Guadeloupe'},
            {iso2: 'GU', name : 'Guam'},
            {iso2: 'GT', name : 'Guatemala'},
            {iso2: 'GN', name : 'Guinea'},
            {iso2: 'GW', name : 'Guinea-Bissau'},
            {iso2: 'GY', name : 'Guyana'},
            {iso2: 'HT', name : 'Haiti'},
            {iso2: 'HM', name : 'Heard Island and Mcdonald Islands'},
            {iso2: 'VA', name : 'Holy See (Vatican City State)'},
            {iso2: 'HN', name : 'Honduras'},
            {iso2: 'HK', name : 'Hong Kong'},
            {iso2: 'HU', name : 'Hungary'},
            {iso2: 'IS', name : 'Iceland'},
            {iso2: 'IN', name : 'India'},
            {iso2: 'ID', name : 'Indonesia'},
            {iso2: 'IR', name : 'Iran, Islamic Republic of'},
            {iso2: 'IQ', name : 'Iraq'},
            {iso2: 'IE', name : 'Ireland'},
            {iso2: 'IL', name : 'Israel'},
            {iso2: 'IT', name : 'Italy'},
            {iso2: 'JM', name : 'Jamaica'},
            {iso2: 'JP', name : 'Japan'},
            {iso2: 'JO', name : 'Jordan'},
            {iso2: 'KZ', name : 'Kazakhstan'},
            {iso2: 'KE', name : 'Kenya'},
            {iso2: 'KI', name : 'Kiribati'},
            {iso2: 'KR', name : 'Korea, Republic of'},
            {iso2: 'KW', name : 'Kuwait'},
            {iso2: 'KG', name : 'Kyrgyzstan'},
            {iso2: 'LV', name : 'Latvia'},
            {iso2: 'LB', name : 'Lebanon'},
            {iso2: 'LS', name : 'Lesotho'},
            {iso2: 'LR', name : 'Liberia'},
            {iso2: 'LY', name : 'Libyan Arab Jamahiriya'},
            {iso2: 'LI', name : 'Liechtenstein'},
            {iso2: 'LT', name : 'Lithuania'},
            {iso2: 'LU', name : 'Luxembourg'},
            {iso2: 'MO', name : 'Macao'},
            {iso2: 'MK', name : 'Macedonia, the Former Yugoslav Republic of'},
            {iso2: 'MG', name : 'Madagascar'},
            {iso2: 'MW', name : 'Malawi'},
            {iso2: 'MY', name : 'Malaysia'},
            {iso2: 'MV', name : 'Maldives'},
            {iso2: 'ML', name : 'Mali'},
            {iso2: 'MT', name : 'Malta'},
            {iso2: 'MH', name : 'Marshall Islands'},
            {iso2: 'MQ', name : 'Martinique'},
            {iso2: 'MR', name : 'Mauritania'},
            {iso2: 'MU', name : 'Mauritius'},
            {iso2: 'YT', name : 'Mayotte'},
            {iso2: 'MX', name : 'Mexico'},
            {iso2: 'FM', name : 'Micronesia, Federated States of'},
            {iso2: 'MD', name : 'Moldova, Republic of'},
            {iso2: 'MC', name : 'Monaco'},
            {iso2: 'MN', name : 'Mongolia'},
            {iso2: 'MS', name : 'Montserrat'},
            {iso2: 'MA', name : 'Morocco'},
            {iso2: 'MZ', name : 'Mozambique'},
            {iso2: 'MM', name : 'Myanmar'},
            {iso2: 'NA', name : 'Namibia'},
            {iso2: 'NR', name : 'Nauru'},
            {iso2: 'NP', name : 'Nepal'},
            {iso2: 'NL', name : 'Netherlands'},
            {iso2: 'AN', name : 'Netherlands Antilles'},
            {iso2: 'NC', name : 'New Caledonia'},
            {iso2: 'NZ', name : 'New Zealand'},
            {iso2: 'NI', name : 'Nicaragua'},
            {iso2: 'NE', name : 'Niger'},
            {iso2: 'NG', name : 'Nigeria'},
            {iso2: 'NU', name : 'Niue'},
            {iso2: 'NF', name : 'Norfolk Island'},
            {iso2: 'MP', name : 'Northern Mariana Islands'},
            {iso2: 'NO', name : 'Norway'},
            {iso2: 'OM', name : 'Oman'},
            {iso2: 'PK', name : 'Pakistan'},
            {iso2: 'PW', name : 'Palau'},
            {iso2: 'PS', name : 'Palestinian Territory, Occupied'},
            {iso2: 'PA', name : 'Panama'},
            {iso2: 'PG', name : 'Papua New Guinea'},
            {iso2: 'PY', name : 'Paraguay'},
            {iso2: 'PE', name : 'Peru'},
            {iso2: 'PH', name : 'Philippines'},
            {iso2: 'PN', name : 'Pitcairn'},
            {iso2: 'PL', name : 'Poland'},
            {iso2: 'PT', name : 'Portugal'},
            {iso2: 'PR', name : 'Puerto Rico'},
            {iso2: 'QA', name : 'Qatar'},
            {iso2: 'RE', name : 'Reunion'},
            {iso2: 'RO', name : 'Romania'},
            {iso2: 'RU', name : 'Russian Federation'},
            {iso2: 'RW', name : 'Rwanda'},
            {iso2: 'SH', name : 'Saint Helena'},
            {iso2: 'KN', name : 'Saint Kitts and Nevis'},
            {iso2: 'LC', name : 'Saint Lucia'},
            {iso2: 'PM', name : 'Saint Pierre and Miquelon'},
            {iso2: 'VC', name : 'Saint Vincent and the Grenadines'},
            {iso2: 'WS', name : 'Samoa'},
            {iso2: 'SM', name : 'San Marino'},
            {iso2: 'ST', name : 'Sao Tome and Principe'},
            {iso2: 'SA', name : 'Saudi Arabia'},
            {iso2: 'SN', name : 'Senegal'},
            {iso2: 'CS', name : 'Serbia and Montenegro'},
            {iso2: 'SC', name : 'Seychelles'},
            {iso2: 'SL', name : 'Sierra Leone'},
            {iso2: 'SG', name : 'Singapore'},
            {iso2: 'SK', name : 'Slovakia'},
            {iso2: 'SI', name : 'Slovenia'},
            {iso2: 'SB', name : 'Solomon Islands'},
            {iso2: 'SO', name : 'Somalia'},
            {iso2: 'ZA', name : 'South Africa'},
            {iso2: 'GS', name : 'South Georgia and the South Sandwich Islands'},
            {iso2: 'ES', name : 'Spain'},
            {iso2: 'LK', name : 'Sri Lanka'},
            {iso2: 'SD', name : 'Sudan'},
            {iso2: 'SR', name : 'Suriname'},
            {iso2: 'SJ', name : 'Svalbard and Jan Mayen'},
            {iso2: 'SZ', name : 'Swaziland'},
            {iso2: 'SE', name : 'Sweden'},
            {iso2: 'CH', name : 'Switzerland'},
            {iso2: 'SY', name : 'Syrian Arab Republic'},
            {iso2: 'TW', name : 'Taiwan'},
            {iso2: 'TJ', name : 'Tajikistan'},
            {iso2: 'TZ', name : 'Tanzania, United Republic of'},
            {iso2: 'TH', name : 'Thailand'},
            {iso2: 'TL', name : 'Timor-Leste'},
            {iso2: 'TG', name : 'Togo'},
            {iso2: 'TK', name : 'Tokelau'},
            {iso2: 'TO', name : 'Tonga'},
            {iso2: 'TT', name : 'Trinidad and Tobago'},
            {iso2: 'TN', name : 'Tunisia'},
            {iso2: 'TR', name : 'Turkey'},
            {iso2: 'TM', name : 'Turkmenistan'},
            {iso2: 'TC', name : 'Turks and Caicos Islands'},
            {iso2: 'TV', name : 'Tuvalu'},
            {iso2: 'UG', name : 'Uganda'},
            {iso2: 'UA', name : 'Ukraine'},
            {iso2: 'AE', name : 'United Arab Emirates'},
            {iso2: 'GB', name : 'United Kingdom'},
            {iso2: 'US', name : 'United States'},
            {iso2: 'UM', name : 'United States Minor Outlying Islands'},
            {iso2: 'UY', name : 'Uruguay'},
            {iso2: 'UZ', name : 'Uzbekistan'},
            {iso2: 'VU', name : 'Vanuatu'},
            {iso2: 'VE', name : 'Venezuela'},
            {iso2: 'VN', name : 'Viet Nam'},
            {iso2: 'VG', name : 'Virgin Islands, British'},
            {iso2: 'VI', name : 'Virgin Islands, U.s.'},
            {iso2: 'WF', name : 'Wallis and Futuna'},
            {iso2: 'EH', name : 'Western Sahara'},
            {iso2: 'YE', name : 'Yemen'},
            {iso2: 'ZM', name : 'Zambia'},
            {iso2: 'ZW', name : 'Zimbabwe'},
            {iso2: 'IM', name : 'Isle of Man'},
            {iso2: 'JE', name : 'Jersey'},
            {iso2: 'GG', name : 'Guernsey'},
            {iso2: 'LA', name : 'Lao People\'s Democratic Republic'},
            {iso2: 'KP', name : 'Korea, Democratic People\'s Republic of'}
        ];
    })
    .service('Logout', ['$http', '$rootScope', '$cookies', '$location', '$timeout', 'API_URL', function($http, $rootScope, $cookies, $location, $timeout, API_URL) {
        return ({doLogout : doLogout});

        function doLogout() {
            $rootScope.globals = {};
            $cookies.remove('globals');
            $http.defaults.headers.common.Authorization = 'Basic';
            $timeout(function() {
                $http.get(API_URL + 'curl-logout').then(function(success) {
                    location.href = '/login';
                }, function(error) {
                    location.href = '/login';
                });
            }, 500);
        }
    }])
    .factory('countryWithRegions', function($q, $http, $injector) {
        var countryLists = [];
        if(countryLists.length > 0) {
            var deferred = $q.defer();
            deferred.resolve(countryLists);
            return deferred.promise;
        } else {
            return $http.get('js/allcountry.json').then(function(result) {
                countryLists = result.data;
                return countryLists;
            });
        }
    })
    .factory('httpLoaderInterceptor', function($q, $injector) {
        return {
            request: request,
            response: response,
            responseError: responseError
        };
        function request(config) {
            //console.log('request', config.url);
            if(config.url.indexOf('api') != -1) {
                rootScope = $injector.get('$rootScope');
                rootScope.showMask = true;
            }
            return config;
        }
        function response(res) {
            //console.log('response', res.config.url);
            if(res.config.url.indexOf('api') != -1) {
                rootScope = $injector.get('$rootScope');
                rootScope.showMask = false;
            }
            return res;
        }
        function responseError(res) {
            console.log('httpLoaderInterceptor', res);
            if(res.config.url.indexOf('api') != -1) {
                rootScope = $injector.get('$rootScope');
                rootScope.showMask = false;
            }
            if(res.status === 401) {
                var Logout = $injector.get('Logout');
                var $timeout = $injector.get('$timeout');
                alert('Unauthorised User access.');
                Logout.doLogout();
            } else if(res.status === -1) {
                //$mdToast.show($mdToast.simple().capsule(true).textContent('Can\'t connect to server. Please check your internet connection.'));
            }
            return $q.reject(res);
        }
    })
    .directive('menuClose', function() {
        return {
            restrict: 'AC',
            link: function($scope, $element) {
                $element.bind('click', function() {
                    var drawer = angular.element(document.querySelector('.mdl-layout__drawer.is-visible'));
                    var obfuscator = angular.element(document.querySelector('.mdl-layout__obfuscator.is-visible'));
                    if(drawer) {
                        drawer.toggleClass('is-visible');
                    }
                    if(obfuscator) {
                        obfuscator.toggleClass('is-visible')
                    }
                });
            }
        };
    });
