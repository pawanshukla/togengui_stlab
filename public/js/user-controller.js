TopengUI
    .controller('UserController', function($scope, $rootScope, $stateParams, $state, UserService, PolicyService, GroupService, $timeout) {
        $rootScope.menuItem = 'user';
        $rootScope.pageTitle = 'Users';
        $scope.userData = {};
        $scope.selectedPolicy = [];
        $scope.selectedGroups = [];
        $scope.groups = [];
        $scope.policies = [];
        $scope.showProgress = false;
        GroupService.get({}, function(success) {
            if(success.success && success.data.length > 0) {
                $scope.groups = success.data.map(function(item) {
                    item.text = item.groupName;
                    return item;
                });
                if($scope.userData.groups && $scope.selectedGroups.length == 0) {
                    $scope.groups.forEach(function(val) {
                        if($scope.userData.groups.indexOf(val.id) != -1) {
                            $scope.selectedGroups.push(val);
                        }
                    });
                }
            }
        }, function(error) {
            console.error(error);
            alert('Unable to get data from server');
        });
        PolicyService.get({}, function(success) {
            if(success.success && success.data.phalicyList.length > 0) {
                $scope.policies = success.data.phalicyList.map(function(item) {
                    item.text = item.name;
                    return item;
                });
                if($scope.userData.policies && $scope.selectedPolicy.length == 0 && $scope.userData.policies.length > 0) {
                    $scope.policies.forEach(function(val) {
                        if($scope.userData.policies.indexOf(val.id) != -1) {
                            $scope.selectedPolicy.push(val);
                        }
                    });
                }
            }
        }, function(error) {
            console.error(error);
            alert('Unable to get data from server');
        });
        $scope.isEdit = true;
        $scope.showEditFormData = function(editId) {
            $scope.editId = editId;
            $scope.isEdit = true;
            $scope.showProgress = true;
            UserService.get({userId : editId}, function(success) {
                $scope.showProgress = false;
                if(success.success) {
                    $scope.userData = success.data;
                    if(success.data.groups) {
                        $scope.groups.forEach(function(val) {
                            if(success.data.groups.indexOf(val.id) != -1) {
                                $scope.selectedGroups.push(val);
                            }
                        });
                    }
                    if(success.data.policies) {
                        $scope.policies.forEach(function(val) {
                            if(success.data.policies.indexOf(val.id) != -1) {
                                $scope.selectedPolicy.push(val);
                            }
                        });
                    }
                    angular.element(document.getElementsByClassName('is-invalid')).addClass('is-dirty').removeClass('is-invalid');
                    $scope.toggleAddBox();
                }
            }, function(error) {
                $scope.showProgress = false;
                console.log(error);
                alert('Unable to get data from server');
                $state.go('users', null, { reload: true });
            });
        };
        $scope.groupSelected = function(group) {
            var addFlag = false;
            $scope.selectedGroups.forEach(function(val) {
                if(val.id == group.id) {
                    addFlag = true;
                    return false;
                }
            });
            if(!addFlag) {
                $scope.selectedGroups.push(group);
            }
        };
        $scope.deleteItem = function(from, id) {
            var indexToDelete;
            $scope['selected' + from].forEach(function(val, index) {
                if(id == val.id) {
                    indexToDelete = index;
                }
            });
            $scope['selected' + from].splice(indexToDelete, 1);
        };
        $scope.policySelected = function(group) {
            var addFlag = false;
            $scope.selectedPolicy.forEach(function(val) {
                if(val.id == group.id) {
                    addFlag = true;
                    return false;
                }
            });
            if(!addFlag) {
                $scope.selectedPolicy.push(group);
            }
        };
        $scope.submitAddUser = function(formData) {
            $scope.showProgressAddEdit = true;
            formData.groups = $scope.selectedGroups.map(function(item) {
                return item.id;
            });
            formData.policies = $scope.selectedPolicy.map(function(item) {
                return item.id;
            });
            formData.id = $scope.editId;
            $scope.userService = new UserService();
            $scope.userService.data = formData;
            if($scope.isEdit) {
                $scope.isEdit = false;
                $scope.userService.userId = $scope.editId;
                $scope.userService.$update(function(success) {
                    $scope.showProgressAddEdit = false;
                    console.log(success);
                    if(success.success) {
                        $scope.toggleAddBox('cancel');
                        $timeout(function() {
                            $scope.loadUsers(1);
                        }, 200);
                    }
                }, function(error) {
                    $scope.showProgressAddEdit = false;
                    console.error(error);
                    alert('Unable to get data from server');
                });
            } else {
                $scope.userService.$save(function(success) {
                    $scope.showProgressAddEdit = false;
                    console.log(success);
                    if(success.success) {
                        $scope.toggleAddBox('cancel');
                        $timeout(function() {
                            $scope.loadUsers(1);
                        }, 200);
                    }
                }, function(error) {
                    $scope.showProgressAddEdit = false;
                    console.error(error);
                    alert('Unable to get data from server');
                });
            }
        };

        // User controller
        $scope.addUserToggle = false;
        $scope.page = 1;
        $scope.noPrevious = true;
        $scope.noNext = false;
        $scope.toggleAddBox = function(source) {
            if(source == 'cancel') {
                $scope.userData = {};
                $scope.selectedPolicy = [];
                $scope.selectedGroups = [];
                $timeout(function() {
                    angular.forEach(angular.element(document.getElementsByClassName('ng-invalid')), function(el) {
                        if(el.tagName.toLowerCase() != 'form') {
                            angular.element(el).parent().addClass('is-invalid').removeClass('is-dirty');
                        }
                    });
                }, 200);
            } else if(source == 'add') {
                $scope.isEdit = false;
            }
            $rootScope.overFlow = !$scope.addUserToggle;
            $scope.addUserToggle = !$scope.addUserToggle;
        };
        $scope.loadUsers = function(pageNo) {
            $scope.showProgress = true;
            UserService.get({'page': pageNo}, function(success) {
                console.log(success);
                $scope.showProgress= false;
                if(success.success) {
                    if(success.data.length == 0) {
                        $scope.page -= 1;
                    }
                    $scope.noNext = success.data.length < parseInt(localStorage.getItem('pageLimit') || 10);
                    $scope.userlist = success;
                }
            }, function(error) {
                console.log(error);
                $scope.showProgress = false;
                alert('Unable to get data from server');
            });
        };
        $scope.callNextPage = function(currentPage) {
            $scope.page += currentPage;
            if($scope.page > 1) {
                $scope.noPrevious = false;
            } else if($scope.page <= 1) {
                $scope.noPrevious = true;
            }
            if($scope.page > 0) {
                $scope.loadUsers($scope.page);
            } else {
                $scope.page = 1;
            }
        };
        $scope.callNextPage(0);
        $scope.deleteUser = function(userid) {
            var deleteElement = angular.element(event.target).parent().parent().parent();
            if(confirm('Are you sure you want to delete user?')) {
                $scope.showProgress = true;
                $scope.userService = new UserService();
                $scope.userService.userId = userid;
                $scope.userService.$delete(function(success) {
                    $scope.showProgress = false;
                    if(success.success) {
                        deleteElement.remove();
                    }
                }, function(error) {
                    $scope.showProgress = false;
                    alert('Unable to get data from server');
                });
            }
        }
    });