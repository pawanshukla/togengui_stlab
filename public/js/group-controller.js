TopengUI
    .controller('GroupsController', function($scope, $rootScope, $stateParams, $state, GroupService, PolicyService, $timeout) {
        $rootScope.menuItem = 'group';
        $rootScope.pageTitle = 'Groups';
        $scope.policies = [];
        $scope.groupData = {};
        $scope.selectedPolicy = [];
        $scope.showProgress = true;
        PolicyService.get({}, function(success) {
            $scope.showProgress = false;
            if(success.success && success.data.phalicyList.length > 0) {
                $scope.policies = success.data.phalicyList.map(function(item) {
                    item.text = item.name;
                    return item;
                });
                if($scope.policies && $scope.selectedPolicy.length == 0 && 'policies' in $scope.groupData && $scope.groupData.policies.length > 0) {
                    $scope.policies.forEach(function(val) {
                        if($scope.groupData.policies.indexOf(val.id) != -1 && $scope.selectedPolicy.indexOf(val.id) == -1) {
                            $scope.selectedPolicy.push(val);
                        }
                    });
                }
            }
        }, function(error) {
            $scope.showProgress = false;
            console.error(error);
            alert('Unable to get data from server');
        });
        $scope.showEditFormData = function(editId) {
            $scope.isEdit = true;
            $scope.editId = editId;
			//alert (JSON.stringify($scope.editId)); 
            $scope.showProgress = true;
            GroupService.get({groupId : $scope.editId}, function(success) {
                $scope.showProgress = false;
				//alert (JSON.stringify(success)); 
                console.log(success);
                if(success.success) {
                    $scope.groupData = success.data;
                    if(success.data.policies) {
                        $scope.policies.forEach(function(val) {
                            if(success.data.policies.indexOf(val.id + '') >= 0) {
                                $scope.selectedPolicy.push(val);
                            }
                        });
                    }
                    angular.element(document.getElementsByClassName('is-invalid')).addClass('is-dirty').removeClass('is-invalid');
                    $scope.toggleAddBox();
                }
            }, function(error) {
                $scope.showProgress = false;
                console.log(error);
                alert('Unable to get data from server');
                $state.go('groups', null, { reload: true });
            });
        };
        $scope.submitAddGroup = function(formData) {
            $scope.showProgressAddUpdate = true;
            formData.policies = $scope.selectedPolicy.map(function(item) {
                return item.id;
            });
            formData.id = $scope.editId;
            $scope.groupService = new GroupService();
            $scope.groupService.data = formData;
            if($scope.isEdit) {
                $scope.groupService.groupId = $scope.editId;
                $scope.groupService.$update(function(success) {
                    $scope.showProgressAddUpdate = false;
                    console.log(success);
                    if(success.success) {
                        $scope.toggleAddBox('cancel');
                        $timeout(function() {
                            $scope.loadGroups(1);
                        }, 200);
                    }
                }, function(error) {
                    $scope.showProgressAddUpdate = false;
                    console.error(error);
                    alert('Unable to get data from server');
                });
            } else {
                $scope.groupService.$save(function(success) {
                    $scope.showProgressAddUpdate = false;
                    console.log(success);
                    if(success.success) {
                        $scope.toggleAddBox('cancel');
                        $timeout(function() {
                            $scope.loadGroups(1);
                        }, 200);
                    }
                }, function(error) {
                    $scope.showProgressAddUpdate = false;
                    console.error(error);
                    alert('Unable to get data from server');
                });
            }
        };
        $scope.deleteItem = function(from, id) {
            console.log(arguments);
            var indexToDelete;
            $scope['selected' + from].forEach(function(val, index) {
                if(id == val.id) {
                    indexToDelete = index;
                }
            });
            $scope['selected' + from].splice(indexToDelete, 1);
        };
        $scope.policySelected = function(group) {
            var addFlag = false;
            $scope.selectedPolicy.forEach(function(val) {
                if(val.id == group.id) {
                    addFlag = true;
                    return false;
                }
            });
            if(!addFlag) {
                $scope.selectedPolicy.push(group)
            }
        };

        // Groups Controller
        $scope.page = 1;
        $scope.noPrevious = true;
        $scope.noNext = false;
        $scope.toggleAddBox = function(source) {
            if(source == 'cancel') {
                $scope.groupData = {};
                $scope.selectedPolicy = [];
                $timeout(function() {
                    angular.forEach(angular.element(document.getElementsByClassName('ng-invalid')), function(el) {
                        if(el.tagName.toLowerCase() != 'form') {
                            angular.element(el).parent().addClass('is-invalid').removeClass('is-dirty');
                        }
                    });
                }, 200);
            } else if(source == 'add') {
                $scope.isEdit = false;
            }
            $rootScope.overFlow = !$scope.addGroupToggle;
            $scope.addGroupToggle = !$scope.addGroupToggle;
        };
        $scope.loadGroups = function(pageNo) {
            $scope.showProgress = true;
            GroupService.get({ page: pageNo}, function(success) {
                $scope.showProgress = false;
                if(success.success) {
                    if(success.data.length == 0) {
                        $scope.page -= 1;
                    }
                    $scope.noNext = success.data.length < parseInt(localStorage.getItem('pageLimit') || 10);
                    $scope.grouplist = success;
                }
            }, function(error) {
                $scope.showProgress = false;
                console.error(error);
                alert('Unable to get data from server');
            });
        };
        $scope.callNextPage = function(currentPage) {
            $scope.page += currentPage;
            if($scope.page > 1) {
                $scope.noPrevious = false;
            } else if($scope.page <= 1) {
                $scope.noPrevious = true;
            }
            if($scope.page > 0) {
                $scope.loadGroups($scope.page);
            } else {
                $scope.page = 1;
            }
        };
        $scope.callNextPage(0);
        $scope.deleteGroup = function(groupid) {
            var deleteElement = angular.element(event.target).parent().parent().parent();
            if(confirm('Are you sure you want to delete group?')) {
                $scope.showProgress = true;
                $scope.groupService = new GroupService();
                $scope.groupService.groupId = groupid;
                $scope.groupService.$delete(function(success) {
                    $scope.showProgress = false;
                    if(success.success) {
                        deleteElement.remove();
                    }
                }, function(error) {
                    $scope.showProgress = false;
                    alert('Unable to get data from server');
                });
            }
        }
    })