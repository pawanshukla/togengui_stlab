$(function() {
    $('#login_form').submit(function(event) {
        var postData = {};
        $(event.target).serializeArray().map(function(obj){
            postData[obj.name] = obj.value;
        });
        $('#loginProgress').fadeIn();
        $.ajax({
            url : '/api/curl-login',
            method: 'post',
            dataType : 'json',
            data : postData,
            success : function(success) {
                $('#loginProgress').fadeOut();
                $('.validation-error').text('');
                if(success.isSuccess) {
                    window.localStorage.setItem('username', $(event.target).find('#userName').val());
                    window.localStorage.setItem('pageLimit', success.page_limit);
                    location.href = '/';
                } else if(success.responseCode == 'phpValidationError') {
                    for(key in success.messages) {
                        $('.mdl-textfield__error.' + key).text(success.messages[key][0]).parent().addClass('is-invalid');
                    }
                }
                console.log('success', success);
            },
            error : function(error) {
                $('#loginProgress').fadeOut();
                $('.validation-error').text('');
                console.log('error', error);
            }
        });
        event.preventDefault();
    });
});