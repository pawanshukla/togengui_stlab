TopengUI
    .controller('OnboardDevicesPerimeterController', function($scope, $rootScope, OnBoardDevicesPerimeterService, $timeout, CountryList, $filter) {
        $rootScope.menuItem = 'onbdevices';
        $rootScope.controllerSubMenu3 = true;
        $rootScope.pageTitle = 'Onboard Devices';

        $scope.countryList = CountryList;
        $scope.submitAddDevice = function(deviceData) {
            var formData = angular.copy(deviceData);
            formData.lastCredUpdate = $filter('date')(formData.lastCredUpdate, 'yyyy-MM-dd HH:mm:ss');
            formData.credUpdateDue = $filter('date')(formData.credUpdateDue, 'yyyy-MM-dd HH:mm:ss');
            formData.valid = formData.valid ? 1 : 0;
            // console.log(formData);return;
            $scope.showProgressAddUpdate = true;
            formData.id = $scope.editId;
            $scope.perimeterService = new OnBoardDevicesPerimeterService();
            $scope.perimeterService.data = formData;
            if($scope.isEdit) {
                $scope.perimeterService.deviceId = $scope.editId;
                $scope.perimeterService.$update(function(success) {
                    $scope.showProgressAddUpdate = false;
                    console.log(success);
                    if(success.success || true) {
                        $scope.toggleAddBox('cancel');
                        $timeout(function() {
                            $scope.loadPeriUsers(1);
                        }, 200);
                    }
                }, function(error) {
                    $scope.showProgressAddUpdate = false;
                    console.error(error);
                    alert('Unable to get data from server');
                });
            } else {
                $scope.perimeterService.$save(function(success) {
                    $scope.showProgressAddUpdate = false;
                    console.log(success);
                    if(success.insertId && success.warningCount == 0) {
                        $scope.toggleAddBox('cancel');
                        $timeout(function() {
                            $scope.loadDevices(1);
                        }, 200);
                    } else {
                        alert('Something went wrong. Unable to create OnBoard Device');
                    }
                }, function(error) {
                    $scope.showProgressAddUpdate = false;
                    console.error(error);
                    alert('Unable to get data from server');
                });
            }
        };

        // Listing related code
        $scope.page = 1;
        $scope.noPrevious = true;
        $scope.noNext = true;
        $scope.resetForm = function() {
            $timeout(function() {
                angular.forEach(angular.element(document.getElementsByClassName('ng-invalid')), function(el) {
                    if(el.tagName.toLowerCase() != 'form') {
                        angular.element(el).parent().addClass('is-invalid').removeClass('is-dirty');
                    }
                });
            }, 200);
        };
        $scope.resetForm();
        $scope.toggleAddBox = function(source) {
            if(source == 'cancel') {
                $scope.userData = {};
                $scope.resetForm();
            } else if(source == 'add') {
                $scope.isEdit = false;
            }
            $rootScope.overFlow = !$scope.addDeviceToggle;
            $scope.addDeviceToggle = !$scope.addDeviceToggle;
        };
        $scope.loadDevices = function(pageNo) {
            $scope.showProgress = true;
            OnBoardDevicesPerimeterService.get({ page: pageNo}, function(success) {
                $scope.showProgress = false;
                if(success.success) {
                    if(success.data.length == 0) {
                        $scope.page -= 1;
                    }
                    $scope.noNext = success.data.length < parseInt(localStorage.getItem('pageLimit') || 10);
                    $scope.devicesList = success;
                }
            }, function(error) {
                $scope.showProgress = false;
                console.error(error);
                alert('Unable to get data from server');
            });
        };
        $scope.callNextPage = function(currentPage) {
            $scope.page += currentPage;
            if($scope.page > 1) {
                $scope.noPrevious = false;
            } else if($scope.page <= 1) {
                $scope.noPrevious = true;
            }
            if($scope.page > 0) {
                $scope.loadDevices($scope.page);
            } else {
                $scope.page = 1;
            }
        };
        $scope.callNextPage(0);
        $scope.deleteDevice = function(sdpId) {
            var deleteElement = angular.element(event.target).parent().parent().parent();
            if(confirm('Are you sure you want to delete user?')) {
                $scope.showProgress = true;
                $scope.perimeterService = new OnBoardDevicesPerimeterService();
                $scope.perimeterService.deviceId = sdpId;
                $scope.perimeterService.$delete(function(success) {
                    $scope.showProgress = false;
                    if(success) {
                        deleteElement.remove();
                    } else {
                        alert('Something went wrong. Unable to delete OnBoard Device.')
                    }
                }, function(error) {
                    $scope.showProgress = false;
                    alert('Unable to get data from server');
                });
            }
        }
    })
    .controller('PerimeterUserController', function($scope, $rootScope, PerimeterUsersService, $timeout, CountryList) {
        $rootScope.menuItem = 'periusers';
        $rootScope.controllerSubMenu3 = true;
        $rootScope.pageTitle = 'Perimeter User';

        $scope.countryList = CountryList;
        $scope.submitAddPerimeterUser = function(formData) {
            // console.log(formData);return;
            $scope.showProgressAddUpdate = true;
            formData.id = $scope.editId;
            $scope.perimeterService = new PerimeterUsersService();
            $scope.perimeterService.data = formData;
            if($scope.isEdit) {
                $scope.perimeterService.userId = $scope.editId;
                $scope.perimeterService.$update(function(success) {
                    $scope.showProgressAddUpdate = false;
                    console.log(success);
                    if(success.success) {
                        $scope.toggleAddBox('cancel');
                        $timeout(function() {
                            $scope.loadPeriUsers(1);
                        }, 200);
                    }
                }, function(error) {
                    $scope.showProgressAddUpdate = false;
                    console.error(error);
                    alert('Unable to get data from server');
                });
            } else {
                $scope.perimeterService.$save(function(success) {
                    $scope.showProgressAddUpdate = false;
                    console.log(success);
                    if(success.success) {
                        $scope.toggleAddBox('cancel');
                        $timeout(function() {
                            $scope.loadPeriUsers(1);
                        }, 200);
                    }
                }, function(error) {
                    $scope.showProgressAddUpdate = false;
                    console.error(error);
                    alert('Unable to get data from server');
                });
            }
        };

        // Listing related code
        $scope.page = 1;
        $scope.noPrevious = true;
        $scope.noNext = true;
        $scope.resetForm = function() {
            $timeout(function() {
                angular.forEach(angular.element(document.getElementsByClassName('ng-invalid')), function(el) {
                    if(el.tagName.toLowerCase() != 'form') {
                        angular.element(el).parent().addClass('is-invalid').removeClass('is-dirty');
                    }
                });
            }, 200);
        };
        $scope.resetForm();
        $scope.toggleAddBox = function(source) {
            if(source == 'cancel') {
                $scope.userData = {};
                $scope.resetForm();
            } else if(source == 'add') {
                $scope.isEdit = false;
            }
            $rootScope.overFlow = !$scope.addPeriUserToggle;
            $scope.addPeriUserToggle = !$scope.addPeriUserToggle;
        };
        $scope.loadPeriUsers = function(pageNo) {
            $scope.showProgress = true;
            PerimeterUsersService.get({ page: pageNo}, function(success) {
                $scope.showProgress = false;
                if(success.success) {
                    if(success.data.length == 0) {
                        $scope.page -= 1;
                    }
                    $scope.noNext = success.data.length < parseInt(localStorage.getItem('pageLimit') || 10);
                    $scope.periUsersList = success;
                }
            }, function(error) {
                $scope.showProgress = false;
                console.error(error);
                alert('Unable to get data from server');
            });
        };
        $scope.callNextPage = function(currentPage) {
            $scope.page += currentPage;
            if($scope.page > 1) {
                $scope.noPrevious = false;
            } else if($scope.page <= 1) {
                $scope.noPrevious = true;
            }
            if($scope.page > 0) {
                $scope.loadPeriUsers($scope.page);
            } else {
                $scope.page = 1;
            }
        };
        $scope.callNextPage(0);
        $scope.deletePeriUser = function(userId) {
            var deleteElement = angular.element(event.target).parent().parent().parent();
            if(confirm('Are you sure you want to delete user?')) {
                $scope.showProgress = true;
                $scope.perimeterService = new PerimeterUsersService();
                $scope.perimeterService.userId = userId;
                $scope.perimeterService.$delete(function(success) {
                    $scope.showProgress = false;
                    if(success.success) {
                        deleteElement.remove();
                    }
                }, function(error) {
                    $scope.showProgress = false;
                    alert('Unable to get data from server');
                });
            }
        }
    })
    .controller('PerimeterGatewaysController', function($scope, $rootScope, PerimeterGatewaysService, $timeout) {
        $rootScope.menuItem = 'perigateway';
        $rootScope.controllerSubMenu3 = true;
        $rootScope.pageTitle = 'Perimeter Gateways';

        $scope.submitAddPerimeterGateWay = function(formData) {
            // console.log(formData);return;
            $scope.showProgressAddUpdate = true;
            formData.id = $scope.editId;
            $scope.perimeterService = new PerimeterGatewaysService();
            $scope.perimeterService.data = formData;
            if($scope.isEdit) {
                $scope.perimeterService.gatewayId = $scope.editId;
                $scope.perimeterService.$update(function(success) {
                    $scope.showProgressAddUpdate = false;
                    console.log(success);
                    if(success.success) {
                        $scope.toggleAddBox('cancel');
                        $timeout(function() {
                            $scope.loadPeriUsers(1);
                        }, 200);
                    }
                }, function(error) {
                    $scope.showProgressAddUpdate = false;
                    console.error(error);
                    alert('Unable to get data from server');
                });
            } else {
                $scope.perimeterService.$save(function(success) {
                    $scope.showProgressAddUpdate = false;
                    console.log(success);
                    if(success.success) {
                        $scope.toggleAddBox('cancel');
                        $timeout(function() {
                            $scope.loadPeriUsers(1);
                        }, 200);
                    }
                }, function(error) {
                    $scope.showProgressAddUpdate = false;
                    console.error(error);
                    alert('Unable to get data from server');
                });
            }
        };

        // Listing related code
        $scope.page = 1;
        $scope.noPrevious = true;
        $scope.noNext = true;
        $scope.resetForm = function() {
            $timeout(function() {
                angular.forEach(angular.element(document.getElementsByClassName('ng-invalid')), function(el) {
                    if(el.tagName.toLowerCase() != 'form') {
                        angular.element(el).parent().addClass('is-invalid').removeClass('is-dirty');
                    }
                });
            }, 200);
        };
        $scope.resetForm();
        $scope.toggleAddBox = function(source) {
            if(source == 'cancel') {
                $scope.gatewayData = {};
                $scope.resetForm();
            } else if(source == 'add') {
                $scope.isEdit = false;
            }
            $rootScope.overFlow = !$scope.addPeriGatewayToggle;
            $scope.addPeriGatewayToggle = !$scope.addPeriGatewayToggle;
        };
        $scope.loadPeriGateways = function(pageNo) {
            $scope.showProgress = true;
            PerimeterGatewaysService.get({ page: pageNo}, function(success) {
                $scope.showProgress = false;
                if(success.success) {
                    if(success.data.length == 0) {
                        $scope.page -= 1;
                    }
                    $scope.noNext = success.data.length < parseInt(localStorage.getItem('pageLimit') || 10);
                    $scope.periGateWaysList = success;
                }
            }, function(error) {
                $scope.showProgress = false;
                console.error(error);
                alert('Unable to get data from server');
            });
        };
        $scope.callNextPage = function(currentPage) {
            $scope.page += currentPage;
            if($scope.page > 1) {
                $scope.noPrevious = false;
            } else if($scope.page <= 1) {
                $scope.noPrevious = true;
            }
            if($scope.page > 0) {
                $scope.loadPeriGateways($scope.page);
            } else {
                $scope.page = 1;
            }
        };
        $scope.callNextPage(0);
        $scope.deletePeriGateway = function(Id) {
            var deleteElement = angular.element(event.target).parent().parent().parent();
            if(confirm('Are you sure you want to delete user?')) {
                $scope.showProgress = true;
                $scope.perimeterService = new PerimeterGatewaysService();
                $scope.perimeterService.gatewayId = Id;
                $scope.perimeterService.$delete(function(success) {
                    $scope.showProgress = false;
                    if(success.success) {
                        deleteElement.remove();
                    }
                }, function(error) {
                    $scope.showProgress = false;
                    alert('Unable to get data from server');
                });
            }
        }
    })
    .controller('PerimeterController', function($scope, $rootScope, PermissionService, $timeout, OnBoardDevicesPerimeterService, PerimeterGatewaysService) {
        $rootScope.menuItem = 'pericontroller';
        $rootScope.controllerSubMenu3 = true;
        $rootScope.pageTitle = 'Permissions';

        $scope.submitAddPermission = function(formData) {
            // console.log(formData);return;
            $scope.showProgressAddUpdate = true;
            formData.id = $scope.editId;
            $scope.permissionService = new PermissionService();
            $scope.permissionService.data = formData;
            if($scope.isEdit) {
                $scope.permissionService.permissionId = $scope.editId;
                $scope.permissionService.$update(function(success) {
                    $scope.showProgressAddUpdate = false;
                    console.log(success);
                    if(success.success) {
                        $scope.toggleAddBox('cancel');
                        $timeout(function() {
                            $scope.loadPeriUsers(1);
                        }, 200);
                    }
                }, function(error) {
                    $scope.showProgressAddUpdate = false;
                    console.error(error);
                    alert('Unable to get data from server');
                });
            } else {
                $scope.permissionService.$save(function(success) {
                    $scope.showProgressAddUpdate = false;
                    console.log(success);
                    if(success.success) {
                        $scope.toggleAddBox('cancel');
                        $timeout(function() {
                            $scope.loadPeriUsers(1);
                        }, 200);
                    }
                }, function(error) {
                    $scope.showProgressAddUpdate = false;
                    console.error(error);
                    alert('Unable to get data from server');
                });
            }
        };

        // Listing related code
        $scope.page = 1;
        $scope.noPrevious = true;
        $scope.noNext = true;
        $scope.resetForm = function() {
            $timeout(function() {
                angular.forEach(angular.element(document.getElementsByClassName('ng-invalid')), function(el) {
                    if(el.tagName.toLowerCase() != 'form') {
                        angular.element(el).parent().addClass('is-invalid').removeClass('is-dirty');
                    }
                });
            }, 200);
        };
        $scope.resetForm();
        $scope.toggleAddBox = function(source) {
            if(source == 'cancel') {
                $scope.gatewayData = {};
                $scope.resetForm();
            } else if(source == 'add') {
                $scope.isEdit = false;
            }
            $rootScope.overFlow = !$scope.addPermissionToggle;
            $scope.addPermissionToggle = !$scope.addPermissionToggle;
        };
        $scope.loadPerimeters = function(pageNo) {
            $scope.showProgress = true;
            PermissionService.get({ page: pageNo}, function(success) {
                $scope.showProgress = false;
                if(success.success) {
                    if(success.data.length == 0) {
                        $scope.page -= 1;
                    }
                    $scope.noNext = success.data.length < parseInt(localStorage.getItem('pageLimit') || 10);
                    $scope.perimeterList = success;
                }
            }, function(error) {
                $scope.showProgress = false;
                console.error(error);
                alert('Unable to get data from server');
            });
        };
        $scope.callNextPage = function(currentPage) {
            $scope.page += currentPage;
            if($scope.page > 1) {
                $scope.noPrevious = false;
            } else if($scope.page <= 1) {
                $scope.noPrevious = true;
            }
            if($scope.page > 0) {
                $scope.loadPerimeters($scope.page);
            } else {
                $scope.page = 1;
            }
        };
        $scope.callNextPage(0);
        $scope.deletePermission = function(Id) {
            var deleteElement = angular.element(event.target).parent().parent().parent();
            if(confirm('Are you sure you want to delete user?')) {
                $scope.showProgress = true;
                $scope.permissionService = new PermissionService();
                $scope.permissionService.permissionId = Id;
                $scope.permissionService.$delete(function(success) {
                    $scope.showProgress = false;
                    if(success.success) {
                        deleteElement.remove();
                    }
                }, function(error) {
                    $scope.showProgress = false;
                    alert('Unable to get data from server');
                });
            }
        };
        OnBoardDevicesPerimeterService.get({}, function(success) {
            $scope.showProgress = false;
            if(success.success) {
                if(success.data.length == 0) {
                    $scope.page -= 1;
                }
                $scope.devicesList = success.data;
            }
        }, function(error) {
            $scope.showProgress = false;
            console.error(error);
            alert('Unable to get Onboard devices from server');
        });
        PerimeterGatewaysService.get({}, function(success) {
            $scope.showProgress = false;
            if(success.success) {
                $scope.periGateWaysList = success.data;
            }
        }, function(error) {
            $scope.showProgress = false;
            console.error(error);
            alert('Unable to get perimeter gateways from server');
        });
    })
    .controller('PerimeterLogController', function($scope, $rootScope, PerimeterAccessLogService) {
        $rootScope.menuItem = 'perimeter-accesslog';
        $rootScope.controllerSubMenu3 = true;
        $rootScope.pageTitle = 'Perimeter Access Logs';
        $scope.accessLogs = [];
        $scope.page = 1;
        $scope.noPrevious = true;
        $scope.noNext = true;
        $scope.filter = {
            // userid : 'all'
        };
        /*UserService.get({}, function(success) {
            if(success.success) {
                $scope.userList = success.data;
            }
        });*/
        $scope.loadAccessLogs = function(page) {
            $scope.filter.page = page;
            $scope.showProgress = true;
            PerimeterAccessLogService.get($scope.filter, function(success) {
                $scope.showProgress = false;
                if(success.success) {
                    if(success.data.length == 0) {
                        $scope.page -= 1;
                    }
                    $scope.noNext = success.data.length < 50;
                    $scope.accessLogs = success.data;
                }
            }, function(err) {
                $scope.showProgress = false;
                console.log(err);
            });
        };
        $scope.callNextPage = function(currentPage) {
            $scope.page += currentPage;
            if($scope.page > 1) {
                $scope.noPrevious = false;
            } else if($scope.page == 1) {
                $scope.noPrevious = true;
            }
            if($scope.page > 0) {
                $scope.loadAccessLogs($scope.page);
            } else {
                $scope.page = 1;
            }
        };
        $scope.callNextPage(0);
        $scope.doFilter = function() {
            $scope.page = 1;
            $scope.callNextPage(0);
        };
    })
