<?php
/**
 * Created by PhpStorm.
 * User: AnkurR
 * Date: Aug 024 24/08/2016
 * Time: 11:58 AM
 */
return [
    /* API server end point */
    'api_server_endpoint' => 'http://ec2-52-36-77-207.us-west-2.compute.amazonaws.com',

    'permission_api_endpoint' => 'http://ec2-34-223-237-208.us-west-2.compute.amazonaws.com:5001',

    /* Data Security Work Flow Controller JAR file link */
    'dswc_jar_url' => 'https://storage.googleapis.com/dev-uploads/PSG-DSWC-0.0.2-SNAPSHOT.jar',

    /* Data Security Server JAR file link */
    'dss_jar_url' => 'https://storage.googleapis.com/dev-uploads/PSG-DSS-0.0.2-SNAPSHOT.jar',

    /* Policy Engine JAR file link */
    'pe_jar_url' => 'https://storage.googleapis.com/dev-uploads/PSG-PE-0.0.2-SNAPSHOT.jar',

    /* SDP RestAPI Endpoint */
    'sdp_rest_endpoint' => 'http://ec2-34-223-237-208.us-west-2.compute.amazonaws.com',
 
    'per_page_limit' => 10
];
