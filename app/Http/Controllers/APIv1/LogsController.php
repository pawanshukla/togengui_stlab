<?php

namespace App\Http\Controllers\APIv1;

use App\Models\Curl;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LogsController extends Controller
{
    private $API_URL = '';
    private $moduleConfigs;
    private $perPageLimit = 50;

    public function __construct()
    {
        $authResponse = json_decode(trim(file_get_contents(config_path().'/authserver.response')));
        $this->API_URL = '';
        if(isset($authResponse->dswc)) {
            $this->moduleConfigs = $dswcData = $authResponse->dswc;
            $this->API_URL = 'https://' . $dswcData->ip . ':' . $dswcData->port . '/dswc/data';
        }
    }

    public function getAuditLogs(Request $request)
    {
        $offset = config('constants.per_page_limit') * ($request->get('page')-1);
        return Curl::request($this->API_URL . '/log?' . http_build_query($request->all()) . '&limit=' . config('constants.per_page_limit') . '&offset=' . $offset, 'GET', '', true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }

    public function getAccessLogs(Request $request)
    {
        if($request->get('limit', 50) != 50) {
            $this->perPageLimit = $request->get('limit');
            $request->request->remove('limit');
        }
        $offset = $this->perPageLimit * ($request->get('page')-1);
        $request->request->remove('page');
        if($request->get('userid') == 'all') {

            $request->request->remove('userid');
        }
        return Curl::request($this->API_URL . '/login/access-logs/get?' . http_build_query($request->all()) . '&limit=' . $this->perPageLimit . '&offset=' . $offset, 'GET', '', true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }

    public function getPolicyViolationLogs(Request $request) {

        if($request->get('limit', 50) != 50) {
            $this->perPageLimit = $request->get('limit');
            $request->request->remove('limit');
        }
        $offset = $this->perPageLimit * ($request->get('page')-1);
        $request->request->remove('page');
        if($request->get('userid') == 'all') {

            $request->request->remove('userid');
        }
        return Curl::request($this->API_URL . '/get/policy-violations?' . http_build_query($request->all()) . '&limit=' . $this->perPageLimit . '&offset=' . $offset, 'GET', '', true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }

    public function getPerimeterAccessLogs() {

        return response()->json(['success' => true, 'data' => json_decode(Curl::request(config('constants.permission_api_endpoint') . '/accessLogAll', 'GET'))]);
    }

}
