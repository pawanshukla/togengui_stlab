<?php

namespace App\Http\Controllers\APIv1;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\AuthAPI;
use App\Models\Curl;
use Illuminate\Http\Request;
use Session;

class PolicyController extends Controller
{
    private $API_URL = '';
    private $moduleConfigs;

    public function __construct() {
        $authResponse = json_decode(trim(file_get_contents(config_path().'/authserver.response')));
        $this->API_URL = '';
        if(isset($authResponse->pe)) {
            $this->moduleConfigs = $peData = $authResponse->pe;
            $this->API_URL = 'https://' . $peData->ip . ':' . $peData->port . '/pe/api/pm/policy/';
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Curl::request($this->API_URL, 'GET', '', true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Curl::request($this->API_URL, 'POST', json_encode($request->get('data')), true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Curl::request($this->API_URL . $id, 'GET', '', true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Curl::request($this->API_URL . $id, 'GET', '', true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return Curl::request($this->API_URL, 'POST', json_encode($request->get('data')), true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Curl::request($this->API_URL . $id, 'DELETE', '', true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }
}
