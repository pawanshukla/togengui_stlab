<?php

namespace App\Http\Controllers\APIv1;

use App\Http\Controllers\Controller;
use App\Models\AuthAPI;
use App\Models\Curl;
use App\Models\Helpers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use Session;
use Redirect;

class CurlUserController extends Controller
{
    public function doLogin(Request $request) {

        // validate the info, create rules for the inputs
        $rules = array(
            'login_username'    => Helpers::getValidationRule('username'), // make sure the email is an actual email
            'login_password' => Helpers::getValidationRule('password') // password can only be alphanumeric and has to be greater than 3 characters
        );
        // run the validation rules on the inputs from the form
        $validator = Validator::make($request->all(), $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return json_encode(['isSuccess' => false, 'responseCode' => 'phpValidationError', 'messages' => $validator->errors()]);
        }
        else {
            $data = array (
                'username' => $request->get('login_username'),
                'password' => $request->get('login_password'),
                'session' => Helpers::getSession(),
                'terminatePreviousSession' => $request->get('terminatePreviousSession')
            );
            $data['isSuccess'] = true;
            $data['page_limit'] = config('constants.per_page_limit');
            $jsonData = json_encode($data);
            Session::set('userSessionData', $jsonData);

            $authFileContent = trim(file_get_contents(config_path().'/authserver.response'));
            if(strlen($authFileContent) == 0) {
                AuthAPI::registerWithAuthServer(AuthAPI::$MODULE_TSGUI, uniqid('ID'));
            }/* else {
                $authResponse = json_decode($authFileContent);
                if(isset($authResponse->dswc) && isset($authResponse->pe)) {
                    session()->put('dswc', $authResponse->dswc);
                    session()->put('pe', $authResponse->pe);
                } else {
                    AuthAPI::registerWithAuthServer(AuthAPI::$MODULE_TSGUI, uniqid('ID'));
                }
            }*/

            return $jsonData;

            $url = env('BASE_URL').'login';
            $login =  Curl::request($url, $jsonData);
            // return $login;

            //calling init web service start
            $data = json_decode($login);
            if($data->isSuccess == 'true'){
                $fields = array (
                    'username' => $data->payload->emailId,
                    'xtoken' => $data->payload->xtoken,
                    'session' => Helpers::getSession()
                );

                $jsonDataInit = json_encode($fields);
                // return $jsonDataInit;
                $url = env('BASE_URL').'init';

                Curl::request($url, $jsonDataInit);
            }
            // calling init web service end

            return $login;
        }
    }

    public function checkAccountExists() {
        $data = array (
            'username' => Input::get('login_password'),
            'session' => Helpers::getSession()
        );
        $jsonData = json_encode($data);
        $url = env('BASE_URL').'checkAccountExists';

        return Curl::request($url, $jsonData);
    }

    public function doRegister() {
        // validate the info, create rules for the inputs
        $rules = array(
            'emailId' => Helpers::getValidationRule('username'), // make sure the email is an actual email
            'password' => Helpers::getValidationRule('password'), // password can only be alphanumeric and has to be greater than 3 characters
            'mobileNumber' => Helpers::getValidationRule('mobileNumber'),
            'firstName' => Helpers::getValidationRule('onlyRequired'),
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return json_encode(['isSuccess' => false, 'responseCode' => 'phpValidationError']);
        }
        else {
            $data = array (
                'email' => Input::get('emailId'),
                'password' => Input::get('password'),
                'firstName' => Input::get('firstName'),
                'lastName' => Input::get('lastName'),
                'mobileNumber' => Input::get('mobileNumber'),
                'session' => Helpers::getSession(),
                'isGroupsRegistration' => false,
                'partnerId' => 0,
                'customerId' => 0,
                'isPaid' => false,
                'hashValue' => 'hashValue',
                'isPartnerRegistration' => false,
                'partnerContactId' => 0,
                'partnerType' => 0,
                'roleId' => intval(Input::get('roleId'))
            );
            // send invite code if any to the backend
            if(Input::get('customerHash') != "")
                $data['inviteCode'] = Input::get('customerHash');

            $jsonData = json_encode($data);
            $url = env('BASE_URL').'register';

            return Curl::request($url, $jsonData);
        }
    }

    public function doForgotPassword() {

        // validate the info, create rules for the inputs
        $rules = array(
            'emailId' => Helpers::getValidationRule('username') // make sure the email is an actual email
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return json_encode(['isSuccess' => false, 'responseCode' => 'phpValidationError']);
        }
        else {
            $fields = array (
                'emailId' => Input::get('emailId')
            );
            $jsonData = json_encode($fields);
            $url = env('BASE_URL').'generateResetPasswordLink';
            return Curl::request($url, $jsonData);
        }
    }

    public function doResetPassword() {
        $password = Input::get('password');
        $passwordConfirm = Input::get('passwordConfirm');

        if($passwordConfirm != $password) {
            return json_encode(['isSuccess' => false, 'responseCode' => 'phpValidationError']);
        }

        // validate the info, create rules for the inputs
        $rules = array(
            'password' => Helpers::getValidationRule('password'),
            'passwordConfirm' => Helpers::getValidationRule('password')
        );

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return json_encode(['isSuccess' => false, 'responseCode' => 'phpValidationError']);
        }
        else {
            $fields = array (
                'password' => $password,
                'token' => Input::get('token')
            );
            $jsonData = json_encode($fields);
            $url = env('BASE_URL').'resetPassword';
            return Curl::request($url, $jsonData);
        }
    }

    public function doActivateAccount() {
        $fields = array(
            'activationToken' => Input::get('token')
        );
        $jsonData = json_encode($fields);
        // return $jsonData;
        $url = env('BASE_URL').'activateAccount';
        return Curl::request($url, $jsonData);
    }

    public function doLogout() {
        if(!Session::get('userSessionData'))
            return Redirect::to(url('/').'login');
        // $userDetails = json_decode(Session::get('userSessionData'));
        // $data = json_encode([
        // 	'username' => $userDetails->payload->emailId,
        // 	'xtoken'   => $userDetails->payload->xtoken,
        // 	'session'  => Helpers::getSession()
        // ]);

        Session::flush();
        // Curl::request( env('BASE_URL').'logout', $data );
        return Redirect::to(url('/').'login');
    }

    public function destroyPreviousSession() {
        $session_id_to_destroy = Input::get('sessionId');
        // 1. commit session if it's started.
        if (session_id()) {
            session_commit();
        }

        // 2. store current session id
        session_start();
        $current_session_id = session_id();
        session_commit();

        // 3. hijack then destroy session specified.
        session_id($session_id_to_destroy);
        session_start();
        session_destroy();
        session_commit();

        // 4. restore current session id. If don't restore it, your current session will refer to the session you just destroyed!
        session_id($current_session_id);
        session_start();
        session_commit();

        return "Destroyed!";
    }
}
