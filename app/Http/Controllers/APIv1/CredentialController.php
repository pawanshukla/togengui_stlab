<?php

namespace App\Http\Controllers\APIv1;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;



// Urls
Route::post('addCredential', 'CredentialController@add');
Route::post('removeCredential', 'CredentialController@remove');

class CredentialController extends Controller
{
    private $authResponse;
    public function __construct()
    {
        $this->authResponse = json_decode(trim(file_get_contents(config_path().'/authserver.response')));
    }

    public function add() {

    }

    public function remove(Request $request) {
        $payloadContent = $request->all();
        var_dump($payloadContent);
        if(isset($this->authResponse->{$payloadContent['module']}) && is_array($this->authResponse->{$payloadContent['module']}) ) {
            foreach ($this->authResponse->{$payloadContent['module']} as $index => $config) {
                if($config->moduleId == $payloadContent['moduleId'] && $config->accessKey == $payloadContent['accessKey']) {
                    unset($this->authResponse->{$payloadContent['module']}[$index]);
                    $this->authResponse->{$payloadContent['module']} = array_values($this->authResponse->{$payloadContent['module']});
                    break;
                }
            }
            file_put_contents(config_path() . '/authserver.response', json_encode($this->authResponse));
        }
    }
}
