<?php

namespace App\Http\Controllers\APIv1;

use App\Models\Curl;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class OnboardDevicesPerimeterController extends Controller
{
	public $API_URL = '';
	
	public function __construct() {
		$this->API_URL = config('constants.permission_api_endpoint');
    }
    
    /**
    * Display a listing of the resource.
    *
    * @param \App\Http\Requests\ $request
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $response = json_decode(Curl::request($this->API_URL . '/perimeterDevices'));
    	return json_encode(['success' => true, 'data' => $response]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->get('data');
        $data['sdpId'] = intval($data['sdpId']);
    	return Curl::request($this->API_URL . '/upSertPerimeterDevice' , 'POST', json_encode($data));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	return Curl::request($this->API_URL . '/perimeterDevice/' . $id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    	return Curl::request($this->API_URL . '/perimeterDevice/' . $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    	return Curl::request($this->API_URL . '/upSertPerimeterDevice/' . $id, 'POST', json_encode($request->get('data')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    	return Curl::request($this->API_URL . '/deletePerimeterDevice/' . $id, 'DELETE');
    }
}
