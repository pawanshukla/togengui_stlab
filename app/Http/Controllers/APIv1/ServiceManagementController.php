<?php

namespace App\Http\Controllers\APIv1;

use App\Models\AuthAPI;
use App\Models\Curl;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;


class ServicesManagementController extends Controller
{
    private $API_URL = '';
//    private $selectedIndex = 0;
    private $moduleConfigs;

    public function __construct() {
        $authResponse = json_decode(trim(file_get_contents(config_path().'/authserver.response')));
        $this->API_URL = '';
        if(isset($authResponse->nsc)) {
            $this->moduleConfigs = $nscData = $authResponse->nsc;
            $this->API_URL = 'https://' . $nscData->ip . ':' . $dswcData->port . '/nsc/services';
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return "hello index";
        return Curl::request($this->API_URL . '/list', 'POST', json_encode([
                'listAll' => true
            ]), true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //print_r("inside store");
        return Curl::request($this->API_URL, 'POST', json_encode($request->get('data')), true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

   /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return Curl::request($this->API_URL, 'POST', json_encode($request->get('data')), true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Curl::request($this->API_URL . '/' . $id, 'DELETE', '', true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }
	
}
