<?php

namespace App\Http\Controllers\APIv1;

use App\Models\AuthAPI;
use App\Models\Curl;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use Socialite;

class DashboardController extends Controller
{
    private $API_URL = '';
//    private $selectedIndex = 0;
    private $moduleConfigs;

    public function __construct() {
        $authResponse = json_decode(trim(file_get_contents(config_path().'/authserver.response')));
        $this->API_URL = '';
        if(isset($authResponse->dswc)) {
            $this->moduleConfigs = $dswcData = $authResponse->dswc;
            $this->API_URL = 'https://' . $dswcData->ip . ':' . $dswcData->port . '/dswc/dashboard';
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authServerConfig = json_decode(file_get_contents(config_path().'/authserver.config'));
        return json_encode(['response' => json_decode(Curl::request('https://'.$authServerConfig->ip . ':'. $authServerConfig->port .'/auth/hb/live?', 'GET', '', true, null, $authServerConfig->username, $authServerConfig->password))]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Curl::request($this->API_URL . '/get?start=' . $request->get('data')['start'] . '&end=' . $request->get('data')['end'], 'GET', '', true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getServerPerformanceReport(Request $request) {
        $authServerConfig = json_decode(file_get_contents(config_path().'/authserver.config'));
        return Curl::request('https://' . $authServerConfig->ip . ':'. $authServerConfig->port . '/auth/modules/performance/avg?end='.$request->get('end').'&start='.$request->get('start'), 'GET', '', true, null, $authServerConfig->username, $authServerConfig->password);
    }

    public function getTopAppByData(Request $request) {

        return Curl::request($this->API_URL . '/get-top-applications/bydata/?start=' . $request->get('start') . '&end=' . $request->get('end'), 'GET', '', true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }

    public function getTopAppByUsers(Request $request) {

        return Curl::request($this->API_URL . '/get-top-applications/byusers/?start=' . $request->get('start') . '&end=' . $request->get('end'), 'GET', '', true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }

    public function getTopDevices(Request $request) {

        return Curl::request($this->API_URL . '/get-top-devices?start=' . $request->get('start') . '&end=' . $request->get('end'), 'GET', '', true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }

    public function getTopLocations(Request $request) {

        return Curl::request($this->API_URL . '/get-top-locations?start=' . $request->get('start') . '&end=' . $request->get('end'), 'GET', '', true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }

    public function getTopTimeOfAccess(Request $request) {

        return Curl::request($this->API_URL . '/get-top-timeofaccess?start=' . $request->get('start') . '&end=' . $request->get('end'), 'GET', '', true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }
	
	
	/**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        //return OAuth::driver('github')->redirect();
		//$user = OAuth::consumer('microsoft');
		    $user =   Socialite::driver('microsoft')->redirect();
		print_r($user);
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        //$user = OAuth::driver('github')->user();

        // $user->token;
		
		$user = Socialite::driver('microsoft')->user(); //OAuth::consumer('microsoft');
			print_r($user);
		// Response from Facebook
		/* if ($code = Input::get('code'))
		{
			$token = $facebook->requestAccessToken($code);
			print_r($token);
			//$result = json_decode($facebook->request('/me'), true);

			//echo 'Your unique facebook user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
		}

		// Redirect to login
		else
		{
			return Redirect::away((string) $facebook->getAuthorizationUri());
		} */
    }

}
