<?php

namespace App\Http\Controllers\APIv1;

use App\Models\AuthAPI;
use App\Models\Curl;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SDKController extends Controller
{
    public $API_URL = '';
    private $authServerConfig = '';

    public function __construct() {
        $this->authServerConfig = json_decode(file_get_contents(config_path().'/authserver.config'));
        $this->API_URL = 'https://' . $this->authServerConfig->ip . ':'. $this->authServerConfig->port .'/auth/api/sdks/';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /* if (session_status() == PHP_SESSION_NONE) {
			session_start();
		}
		$tokenCache = new \App\TokenStore\TokenCache;
		$token_value = $tokenCache->getAccessToken(); */
		
		return Curl::request($this->API_URL, 'GET', '', true, null, $this->authServerConfig->username, $this->authServerConfig->password);
		
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->get('data')['action'] == 'delete') {
            return Curl::request($this->API_URL . $request->get('data')['module'] . '/'. $request->get('data')['moduleId'], 'DELETE', '', true, null, $this->authServerConfig->username, $this->authServerConfig->password);
        } else {
            return Curl::request($this->API_URL . $request->get('data')['module'] . '/'. $request->get('data')['moduleId'], 'POST', json_encode([
                'name' => $request->get('data')['name']
            ]), true, null, $this->authServerConfig->username, $this->authServerConfig->password);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id )
    {
		//$id = '94c59822-0af1-449c-9957-48c321697c26';
		//print_r($this->API_URL  . 'get' . '?' . 'module=' . '&moduleId="'. $id.'"');
		 //return $ip = $this->API_URL  . 'get' . '?' . 'module=sdk' . '&moduleId=' . $id ;
	    //return Curl::request($this->API_URL  . 'get' . '?' . 'module=' . '&moduleId="' . $id.'"', 'GET', '', true, null, $this->authServerConfig->username, $this->authServerConfig->password);
		//return Curl::request($this->API_URL . $id, 'GET', '', true, null, $this->authServerConfig->username, $this->authServerConfig->password);
		return Curl::request($this->API_URL . 'get?' . http_build_query($request->all()) , 'GET', '', true, null, $this->authServerConfig->username, $this->authServerConfig->password);
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //print_r($this->API_URL  . 'get' . '?' . 'module=' . '&moduleId="'. $id."'");
		//return Curl::request($this->API_URL  . 'get' . '?' . 'module=' . '&moduleId=' . $id, 'GET', json_encode($request->get('data')), true, null, $this->authServerConfig->username, $this->authServerConfig->password);
		return Curl::request($this->API_URL . $id, 'GET', '', true, null, $this->authServerConfig->username, $this->authServerConfig->password);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     
    public function update(Request $request, $id)
    {
        //
	}
		*/
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	public function update($token_value,$moduleId)
    {
        
		$data = ['name'=>'OneDrive-Connector','additionalParams'=>$token_value];
		$tokenValue = json_encode($data);
		return Curl::request($this->API_URL.'sdk/'.$moduleId, 'POST', $tokenValue, true, null, $this->authServerConfig->username, $this->authServerConfig->password);
		
    }
}
