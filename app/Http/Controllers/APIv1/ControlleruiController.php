<?php

namespace App\Http\Controllers\APIv1;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Session;

class ControlleruiController extends Controller
{
    public $API_URL = '';

    public function __construct() {
        $this->API_URL = config('constants.api_server_endpoint') . ':8060/api/pm/controllerui/';
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $destination = getcwd() . '/../uploads/';
        $pemDestination = $destination . 'pems/';
        $jarDestination = $destination . 'jars/';
        chdir($jarDestination);

        $DSSDeployed = false;
        if($request->file('serverFileDSS')) {
            $DSSFile = 'DSS_' . $request->input('serverUsernameDSS') . '_' . $request->input('serverIpDSS') . '_' . $request->file('serverFileDSS')->getClientOriginalName();
            $request->file('serverFileDSS')->move($pemDestination, $DSSFile);
            $url = explode('/', config('constants.dss_jar_url'));
            $jarFile = array_pop($url);
            if(!file_exists($jarDestination . $jarFile)) {
                file_put_contents($jarFile, fopen(config('constants.dss_jar_url'), 'r'));
            }

            $ssh = new \Net_SSH2($request->input('serverIpDSS'), 22);
            $key = new \Crypt_RSA();

            $key->loadKey(file_get_contents($pemDestination . $DSSFile));

            if(!$ssh->login($request->input('serverUsernameDSS'), $key)) {
                $DSSDeployed = false;
            } else {
                $scp = new \Net_SCP($ssh);
                $scp->put($jarFile, $jarDestination.$jarFile, NET_SCP_LOCAL_FILE);

                $ssh->exec('nohup java -jar '. $jarFile);

                if(strlen($ssh->exec('fuser 8070/tcp')) > 0) {
                    $DSSDeployed = true;
                }
                Session::set('DSSUrl', $request->input('serverIpDSS'));
            }

        }

        $PEDeployed = false;
        if($request->file('serverFilePE')) {
            $PEFile = 'PE_' . $request->input('serverUsernamePE') . '_' . $request->input('serverIpPE') . '_' . $request->file('serverFilePE')->getClientOriginalName();
            $request->file('serverFilePE')->move($pemDestination, $PEFile);
            $url = explode('/', config('constants.pe_jar_url'));
            $jarFile = array_pop($url);
            if(!file_exists($jarDestination . $jarFile)) {
                file_put_contents($jarFile, fopen(config('constants.pe_jar_url'), 'r'));
            }
            $ssh = new \Net_SSH2($request->input('serverIpPE'), 22);
            $key = new \Crypt_RSA();

            $key->loadKey(file_get_contents($pemDestination . $PEFile));

            if(!$ssh->login($request->input('serverUsernamePE'), $key)) {
                $PEDeployed = false;
            } else {
                $ssh->exec('mkdir -p mongodb');
                $path = trim($ssh->exec('cd mongodb && pwd'), " \n");
                if(strpos($ssh->exec('mongod --version'), 'db version') === false) { // Install Mongo DB if not installed
                    $ssh->exec('curl -O https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-3.2.9.tgz');
                    $ssh->exec('tar -zxvf mongodb-linux-x86_64-3.2.9.tgz');
                    $ssh->exec('sudo mkdir -p /data/db');
                    $ssh->exec('sudo chown -R mongodb:mongodb /data/db');
                    $ssh->exec('cp -R mongodb-linux-x86_64-3.2.9/* mongodb');
                    $ssh->exec('sudo ' . $path . '/bin/mongod');
                }
                if(strpos($ssh->exec('service mongod status'), 'running') === false) {
                    $ssh->exec('sudo ' . $path . '/bin/mongod');
                }

                $scp = new \Net_SCP($ssh);
                $scp->put($jarFile, $jarDestination.$jarFile, NET_SCP_LOCAL_FILE);

                $ssh->exec('nohup java -jar '. $jarFile);
                if(strlen($ssh->exec('fuser 8060/tcp')) > 0) {
                    $PEDeployed = true;
                }
                Session::set('PEUrl', $request->input('serverIpPE'));
            }
        }

        $DSWFCDeployed = false;
        if($request->file('serverFileDSWFC')) {
            $DSWFCFile = 'DSWFC_' . $request->input('serverUsernameDSWFC') . '_' . $request->input('serverIpDSWFC') . '_' . $request->file('serverFileDSWFC')->getClientOriginalName();
            $request->file('serverFileDSWFC')->move($pemDestination, $DSWFCFile);
            $url = explode('/', config('constants.dswc_jar_url'));
            $jarFile = array_pop($url);
            if(!file_exists($jarDestination . $jarFile)) {
                file_put_contents($jarFile, fopen(config('constants.dswc_jar_url'), 'r'));
            }
            $ssh = new \Net_SSH2($request->input('serverIpDSWFC'), 22);
            $key = new \Crypt_RSA();

            $key->loadKey(file_get_contents($pemDestination . $DSWFCFile));

            if(!$ssh->login($request->input('serverUsernameDSWFC'), $key)) {
                $DSWFCDeployed = false;
            } else {

                if($request->input('serverIpDSWFC') != $request->input('serverIpPE')) {
                    $ssh->exec('mkdir -p mongodb');
                    $path = trim($ssh->exec('cd mongodb && pwd'), " \n");
                    if(strpos($ssh->exec('mongod --version'), 'db version') === false) { // Install Mongo DB if not installed
                        $ssh->exec('curl -O https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-3.2.9.tgz');
                        $ssh->exec('tar -zxvf mongodb-linux-x86_64-3.2.9.tgz');
                        $ssh->exec('sudo mkdir -p /data/db');
                        $ssh->exec('sudo chown -R mongodb:mongodb /data/db');
                        $ssh->exec('cp -R mongodb-linux-x86_64-3.2.9/* mongodb');
                        $ssh->exec('sudo ' . $path . '/bin/mongod');
                    }
                    if(strpos($ssh->exec('service mongod status'), 'running') === false) {
                        $ssh->exec('sudo ' . $path . '/bin/mongod');
                    }
                }
                $scp = new \Net_SCP($ssh);
                $scp->put($jarFile, $jarDestination.$jarFile, NET_SCP_LOCAL_FILE);

                $url = $request->input('serverIpDSWFC');
                $fileContent = <<<FILE
    httpport=8080
    dssDecrypt=http://$url:8070/dss/decrypt
    dssEncrypt=http://$url:8070/dss/encrypt
    dssTokenize=http://$url:8070/dss/tokenize
    peActionsFromAttributes=http://$url:8060/api/am/action
    peAttributeList=http://$url:8060/api/am/dataattribute
    pePolicyList==http://$url:8060/api/am/policy
FILE;
                $ssh->exec('echo "' . $fileContent . '" | cat > .topeng.dswc.config');
                $ssh->exec('nohup java -jar '. $jarFile);
                if(strlen($ssh->exec('fuser 8080/tcp')) > 0) {
                    $DSWFCDeployed = true;
                }
                Session::set('DSWFCUrl', $request->input('serverIpDSWFC'));
            }

        }
        echo json_encode(array('success' => true, 'DSS' => $DSSDeployed, 'PE' => $PEDeployed, 'DSWFC' => $DSWFCDeployed, 'DSSFile' => $DSSFile, 'PEFile' => $PEFile, 'DSWFCFile' => $DSWFCFile));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $destination = getcwd() . '/../uploads/';
        $pemDestination = $destination . 'pems/';

        // DSS
        $ssh = new \Net_SSH2($request->input('serverIpDSS'), 22);
        $key = new \Crypt_RSA();

        $key->loadKey(file_get_contents($pemDestination . $request->input('serverFileDSS')));

        if(!$ssh->login($request->input('serverUsernameDSS'), $key)) {
            $DSSDeployed = false;
        } else {
            $DSSPid = array_filter(explode(' ', trim($ssh->exec('fuser 8070/tcp'), " \n")));
            $ssh->exec('rm -rf *.jar .topeng.dswc.config');
            $ssh->exec('kill -9 ' . array_pop($DSSPid));
            $DSSDeployed = true;
        }

        // PE
        $ssh = new \Net_SSH2($request->input('serverIpPE'), 22);
        $key = new \Crypt_RSA();

        $key->loadKey(file_get_contents($pemDestination . $request->input('serverFilePE')));

        if(!$ssh->login($request->input('serverUsernamePE'), $key)) {
            $PEDeployed = false;
        } else {
            $PEPid = array_filter(explode(' ', trim($ssh->exec('fuser 8060/tcp'), " \n")));
            $ssh->exec('rm -rf *.jar .topeng.dswc.config');
            $ssh->exec('kill -9 ' . array_pop($PEPid));
            $PEDeployed = true;
        }

        // DSWFC
        $ssh = new \Net_SSH2($request->input('serverIpDSWFC'), 22);
        $key = new \Crypt_RSA();

        $key->loadKey(file_get_contents($pemDestination . $request->input('serverFileDSWFC')));

        if(!$ssh->login($request->input('serverUsernameDSWFC'), $key)) {
            $DSWFCDeployed = false;
        } else {
            $DSWFCPid = array_filter(explode(' ', trim($ssh->exec('fuser 8080/tcp'), " \n")));
            $ssh->exec('rm -rf *.jar .topeng.dswc.config');
            $ssh->exec('kill -9 ' . array_pop($DSWFCPid));
            $DSWFCDeployed = true;
        }
        Session::forget('DSWFCUrl');
        Session::forget('PEUrl');
        echo json_encode(array('success' => true, 'DSS' => $DSSDeployed, 'PE' => $PEDeployed, 'DSWFC' => $DSWFCDeployed));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
    }
}
