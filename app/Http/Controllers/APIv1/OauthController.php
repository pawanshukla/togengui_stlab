<?php

namespace App\Http\Controllers\APIv1;

use App\Models\AuthAPI;
use App\Models\Curl;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;

use Socialite;

class OauthController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('microsoft')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('microsoft')->user();
		print_r($user);
        // $user->token;
    }
}