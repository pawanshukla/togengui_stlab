<?php

namespace App\Http\Controllers\APIv1;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\AuthAPI;
use App\Models\Curl;
use Illuminate\Http\Request;
use Session;
use Socialite;

class UserController extends Controller
{
    private $API_URL = '';
    private $moduleConfigs;

    public function __construct() {
		
        //$user =   Socialite::driver('microsoft')->redirect();
		//print_r($user);
		$authResponse = json_decode(trim(file_get_contents(config_path().'/authserver.response')));
        $this->API_URL = '';
        if(isset($authResponse->dswc)) {
            $this->moduleConfigs = $dswcData = $authResponse->dswc;
            $this->API_URL = 'https://' . $dswcData->ip . ':' . $dswcData->port . '/dswc/users/';
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param \App\Http\Requests\ $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		
	   $offset = config('constants.per_page_limit') * ($request->get('page')-1);
        return Curl::request($this->API_URL . 'list?limit=' . config('constants.per_page_limit') . '&offset=' . $offset, 'GET', '', true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Curl::request($this->API_URL . 'create', 'POST', json_encode($request->get('data')), true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Curl::request($this->API_URL . $id, 'GET', '', true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Curl::request($this->API_URL . $id, 'GET', '', true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return Curl::request($this->API_URL . $id, 'POST', json_encode($request->get('data')), true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return Curl::request($this->API_URL . $id, 'DELETE', '', true, null, $this->moduleConfigs->username, $this->moduleConfigs->password);
    }
	
	public function redirectToProvider()
    {
        //return OAuth::driver('github')->redirect();
		//$user = OAuth::consumer('microsoft');
		    $user =   Socialite::driver('microsoft')->redirect();
		print_r($user);
		exit;
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        //$user = OAuth::driver('github')->user();

        // $user->token;
		
		$user = Socialite::driver('microsoft')->user(); //OAuth::consumer('microsoft');
			print_r($user);
			exit;
		// Response from Facebook
		/* if ($code = Input::get('code'))
		{
			$token = $facebook->requestAccessToken($code);
			print_r($token);
			//$result = json_decode($facebook->request('/me'), true);

			//echo 'Your unique facebook user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
		}

		// Redirect to login
		else
		{
			return Redirect::away((string) $facebook->getAuthorizationUri());
		} */
    }
}
