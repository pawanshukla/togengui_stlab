<?php

namespace App\Http\Controllers\APIv1;

use App\Models\AuthAPI;
use App\Models\Curl;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class IpwhitelistController extends Controller
{
    public $API_URL = '';
    private $authServerConfig = '';

    public function __construct() {
        $this->authServerConfig = json_decode(file_get_contents(config_path().'/authserver.config'));
        $this->API_URL = 'https://' . $this->authServerConfig->ip . ':'. $this->authServerConfig->port .'/auth/api/whitelisting/';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Curl::request($this->API_URL . 'list?', 'GET', '', true, null, $this->authServerConfig->username, $this->authServerConfig->password);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($this->valid_ip_cidr($request->get('data')['subnet'])) {
            $data  = 'module=' . $request->get('data')['module'] . '&moduleId='.$request->get('data')['moduleId'] . '&subnet='.urlencode($request->get('data')['subnet']);
            if($request->get('data')['action'] == 'delete') {
                return Curl::request($this->API_URL . 'delete?' . $data, 'POST', '', true, null, $this->authServerConfig->username, $this->authServerConfig->password);
            } else {
                return Curl::request($this->API_URL . 'add?' . $data, 'POST', '', true, null, $this->authServerConfig->username, $this->authServerConfig->password);
            }
        } else {
            return json_encode(['success' => false, 'msg' => 'Invalid subnet']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //
    }

    private function valid_ip_cidr($cidr, $must_cidr = false)
    {
        if (!preg_match("/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}(\/[0-9]{1,2})?$/", $cidr))
        {
            $return = false;
        } else
        {
            $return = true;
        }
        if ($return == true)
        {
            $parts = explode("/", $cidr);
            $ip = $parts[0];
            $netmask = $parts[1];
            $octets = explode(".", $ip);
            foreach ($octets as $octet)
            {
                if ($octet > 255)
                {
                    $return = false;
                }
            }
            if ((($netmask != "") && ($netmask > 32) && !$must_cidr) || (($netmask == ""||$netmask > 32) && $must_cidr))
            {
                $return = false;
            }
        }
        return $return;
    }
}
