<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('login', function () {
    return view('login');
});

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', function () {
        return view('dashboard');
    });
});

Route::get('microsoft', 'OauthController@redirectToProvider');
Route::get('oauth/microsoft/callback', 'OauthController@handleProviderCallback');
Route::get('/signin/{moduleId}', 'AuthController@signin');
Route::get('/authorize', 'AuthController@gettoken');
Route::get('/mail', 'OutlookController@mail')->name('mail');
Route::group(['namespace' => 'APIv1', 'prefix' => 'api'], function() {
    Route::post('curl-login', 'CurlUserController@doLogin');
    Route::get('curl-logout', 'CurlUserController@doLogout');
    Route::group(['middleware' => 'auth'], function () {
        Route::resource('user', 'UserController');
        Route::resource('group', 'GroupController');
        Route::resource('policy', 'PolicyController');
        Route::get('audit', 'LogsController@getAuditLogs');
        Route::get('accesslog', 'LogsController@getAccessLogs');
        Route::get('policylog', 'LogsController@getPolicyViolationLogs');
        Route::get('perimeteraccesslog', 'LogsController@getPerimeterAccessLogs');
        Route::resource('deployment', 'ControlleruiController');
        Route::get('dashboard/performanceReport', 'DashboardController@getServerPerformanceReport');
        Route::get('dashboard/getTopAppByData', 'DashboardController@getTopAppByData');
        Route::get('dashboard/getTopAppByUsers', 'DashboardController@getTopAppByUsers');
        Route::get('dashboard/getTopDevices', 'DashboardController@getTopDevices');
        Route::get('dashboard/getTopLocations', 'DashboardController@getTopLocations');
        Route::get('dashboard/getTopTimeOfAccess', 'DashboardController@getTopTimeOfAccess');
		Route::get('dashboard/microsoft', 'DashboardController@handleProviderCallback');
        Route::resource('dashboard', 'DashboardController');
		Route::resource('onedrive', 'OnedriveController');
        Route::resource('standard', 'StandardController');
        Route::resource('ipwhitelist', 'IpwhitelistController');
        Route::resource('attribute', 'AttributeController');
        Route::resource('servicemanagement', 'ServiceManagementController');
        Route::resource('sdk', 'SDKController');
        Route::resource('importdata', 'ImportController');
        Route::resource('perimeter', 'PermissionController');
        Route::resource('onboarddevices', 'OnboardDevicesPerimeterController');
        Route::resource('perimeterusers', 'PerimeterUserController');
        Route::resource('perimetergateways', 'PerimeterGatewaysController');
    });

});
