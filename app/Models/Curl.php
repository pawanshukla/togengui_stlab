<?php

namespace App\Models;

class Curl
{
    public function __construct() {
        Session::regenerate();
    }

    public static function request($url, $method = 'GET', $data = '', $response = true, $timeout = null, $username = null, $password = null) {
        if(defined('EP'))
            error_log($url . "\n", 3, EP);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, $response);

        if($username && $password) {
            curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
        }

        if($timeout) {
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        }
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data)
        ]);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        $result = curl_exec($ch);
        if(defined('EP'))
            error_log('Curl Response : ' . $result . "\n\n\n", 3, EP);
        if ($result === FALSE) {
            printf("cUrl error (#%d): %s<br>\n", curl_errno($ch),
                htmlspecialchars(curl_error($ch)));
        }
        curl_close($ch);
        return $result;
    }
}
