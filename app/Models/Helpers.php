<?php
/**
 * Created by PhpStorm.
 * User: AnkurR
 * Date: Jun 030 6/30/2016
 * Time: 12:15 PM
 */

namespace App\Models;

use Session;
class Helpers {
    public static function getSession() {
        return [
            'ip' => $_SERVER['REMOTE_ADDR'],
            'port' => $_SERVER['REMOTE_PORT'],
            'sessionid' => Session::get('_token'),
            'appid' => env('APP_ID'),
            'hash' => hash_hmac('sha256', env('APP_ID'), env('APP_ID').'|'.env('APP_SECRET'))
        ];
    }

    public static function getValidationRule($validationCase) {
        switch($validationCase) {
            case 'username':
                return 'required|email';
            case 'password':
                return array('required','between:8,30','regex:$\S*(?=\S{8,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])(?=\S*[\W])\S*$');
            case 'mobileNumber' :
                return 'required|digits_between:10,20';
            case 'onlyRequired' :
                return 'required';
            case 'websiteUrl' :
                return 'url';
        }
    }

    public static function isJson($string) {
        if($string == '' || $string == null)
            return false;
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
} 