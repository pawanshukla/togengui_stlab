<?php

namespace App\Models;

use App\Events\Event;
use Illuminate\Database\Eloquent\Model;
use App\Models\Curl;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\URL;
use MongoDB\Driver\Exception\ExecutionTimeoutException;

class AuthAPI extends Model
{
    public static $MODULE_DSS = 'dss';
    public static $MODULE_DSWC = 'dswc';
    public static $MODULE_PE = 'pe';
    public static $MODULE_SDK = 'sdk';
    public static $MODULE_TSGUI = 'tsgui';
    public static $MODULE_AUTH = 'auth';
    public static $MODULE_TPC = "tpc";
    public static $MODULE_TPG = "tpg";
    public static $MODULE_NSC = 'nsc';
    private static $authServer;

    public static function registerWithAuthServer($module, $moduleId) {
        $authServer = self::getAuthServer();
        $registrationObj = self::getRegistrationDTO($module, $moduleId);
        return self::register($authServer, $registrationObj);
    }

    public static function register($authServer, RegistrationDTO $registrationObj) {
        $url = 'https://' . $authServer->ip . ':' . $authServer->port.'/auth/api/register';
        $response = json_decode(Curl::request($url, 'POST', $registrationObj->getSerialised(), true, null, $authServer->username, $authServer->password));
        if($response->success) {
            $saveData = [];
            if(count($response->data) > 0) {
                foreach ($response->data as $module) {
                    $saveData[$module->module] = $module;
                }
            }
            file_put_contents(config_path() . '/authserver.response', json_encode($saveData));
        }

        return $response->success;
    }

    public static function calculateSignature($accessId, $timestamp, $passKey) {
        $regObjString='timestamp='.$timestamp.'&accessId='.$accessId;
        return self::generateSignature($passKey, $regObjString . '::' . $timestamp);
    }

    public static function generateSignature($passkey, $data) {
        return bin2hex(hash_hmac('sha256', $data, $passkey, true));
    }

    public static function getRegistrationDTO($module, $moduleId) {
        $dto = new RegistrationDTO();
        $dto->setIp(request()->ip());
        $dto->setPort(80);
        $dto->setModule($module);
        $dto->setModuleId($moduleId);
        return $dto;
    }

    public static function getAuthServer() {
        if(self::$authServer == null)  {
            self::$authServer = json_decode(file_get_contents(config_path().'/authserver.config'));
        }
        return self::$authServer;
    }

    public static function getSignatureData($module, $index = 0) {
        $authResponse = json_decode(trim(file_get_contents(config_path().'/authserver.response')));
        if(isset($authResponse->{$module}[$index])) {
            $timestamp = time() * 1000;
            if(defined('EP'))
                error_log(print_r($authResponse->{$module}[$index], true), 3, EP);
            $signature = self::calculateSignature($authResponse->{$module}[$index]->accessKey, $timestamp, $authResponse->{$module}[$index]->passKey);
            return 'signature=' . $signature . '&accessId=' . $authResponse->{$module}[$index]->accessKey . '&timestamp=' . $timestamp;
        }
        return '';
    }
}

class RegistrationDTO {
    private $ip;
    private $port;
    private $module;
    private $moduleId;

    public function getIp() {
        return $this->ip;
    }
    public function setIp($ip) {
        $this->ip = $ip;
    }
    public function getPort() {
        return $this->port;
    }
    public function setPort($port) {
        $this->port = $port;
    }
    public function getModule() {
        return $this->module;
    }
    public function setModule($module) {
        $this->module = $module;
    }
    public function getModuleId() {
        return $this->moduleId;
    }
    public function setModuleId($moduleId) {
        $this->moduleId = $moduleId;
    }

    public function getSerialised() {
        return json_encode([
            'ip' => $this->getIp(),
            'port'  => $this->getPort(),
            'module' => $this->getModule(),
            'moduleId' => $this->getModuleId()
        ]);
    }
}
