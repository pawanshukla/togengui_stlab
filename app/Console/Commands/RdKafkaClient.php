<?php

namespace App\Console\Commands;

use App\Models\AuthAPI;
use Illuminate\Console\Command;

class RdKafkaClient extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'rdkafka:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $authFileContent = trim(file_get_contents(config_path().'/authserver.response'));
        if(strlen($authFileContent) > 0) {
            $authResponse = json_decode($authFileContent);

            $conf = new \RdKafka\Conf();

            $conf->set('group.id', 'group5');
            $this->comment('End Point : ' . $authResponse->tsgui->channelBootstrap);
            $conf->set('metadata.broker.list', $authResponse->tsgui->channelBootstrap);

            $this->comment('Channel : ' . $authResponse->tsgui->messageChannel);
            $consumer = new \RdKafka\KafkaConsumer($conf);
            $consumer->subscribe([$authResponse->tsgui->messageChannel]);

            $this->info("Waiting for partition assignment... (make take some time when");
            $this->info("quickly re-joining the group after leaving it.)");


            while (true) {
                $message = $consumer->consume(120*1000);
                switch ($message->err) {
                    case RD_KAFKA_RESP_ERR_NO_ERROR:
                        var_dump($message);
                        $payload = json_decode($message->payload);
                        switch ($payload->objectClass) {
                            case 'ServerDetailsKeyStore':
                                $payloadContent = json_decode($payload->objectContent);
                                if(isset($authResponse->{$payloadContent->module}) && is_array($authResponse->{$payloadContent->module}) && count($payloadContent) > 0) {
                                    array_push($authResponse->{$payloadContent->module}, $payloadContent);
                                } else if(count($payloadContent) > 0) {
                                    $authResponse->{$payloadContent->module} = [$payloadContent];
                                }
                                file_put_contents(config_path() . '/authserver.response', json_encode($authResponse));
                                break;

                            case 'HeIsDead':
                                $payloadContent = json_decode($payload->objectContent);
                                var_dump($payloadContent);
                                if(isset($authResponse->{$payloadContent->module}) && is_array($authResponse->{$payloadContent->module}) ) {
                                    foreach ($authResponse->{$payloadContent->module} as $index => $config) {
                                        if($config->moduleId == $payloadContent->moduleId && $config->accessKey == $payloadContent->accessKey) {
                                            unset($authResponse->{$payloadContent->module}[$index]);
                                            $authResponse->{$payloadContent->module} = array_values($authResponse->{$payloadContent->module});
                                            break;
                                        }
                                    }
                                    file_put_contents(config_path() . '/authserver.response', json_encode($authResponse));
                                }
                                break;
                        }
                        break;
                    case RD_KAFKA_RESP_ERR__PARTITION_EOF:
                        if(!isset($authResponse->pe) || !isset($authResponse->dswc) || (isset($authResponse->pe) && count($authResponse->pe) == 0) || (isset($authResponse->dswc) && count($authResponse->dswc) == 0)) {
                            $this->comment("Producing Message");
                            $this->produceMessage($authResponse->tsgui->channelBootstrap, $authResponse->tsgui->moduleId);
                        }
                        $this->warn("No more messages; will wait for more");
                        break;
                    case RD_KAFKA_RESP_ERR__TIMED_OUT:
                        $this->error("Timed out");
                        break;
                    default:
                        throw new \Exception($message->errstr(), $message->err);
                        break;
                }
            }
        }

    }

    public function produceMessage($endPoint, $moduleId) {
        $rk = new \RdKafka\Producer();

        $this->comment('Producer End point : ' . $endPoint);
        $rk->addBrokers($endPoint);

        $authServerConfigs = json_decode(file_get_contents(config_path().'/authserver.config'));

        $this->comment('Producer Channel : ' . $authServerConfigs->messageChannel);
        $topic = $rk->newTopic($authServerConfigs->messageChannel);

        $this->comment('Producer Module Id : ' . $moduleId);
        $topic->produce(RD_KAFKA_PARTITION_UA, 0, '{"objectClass":"SendCredentialsRequest","objectContent":"{\"module\":\"'. AuthAPI::$MODULE_TSGUI .'\",\"moduleId\":\"'.$moduleId.'\"}"}');

        $rk->poll(0);
    }
}
