<?php

namespace App\Console\Commands;

use App\Models\AuthAPI;
use Illuminate\Console\Command;

class AuthRegister extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'register:auth';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $authFileContent = trim(file_get_contents(config_path().'/authserver.response'));
        if(strlen($authFileContent) == 0) {
            $uuid = file_get_contents(config_path().'/authserver.uuid');
            if(!$uuid) {
                $uuid = uniqid('ID');
                file_put_contents(config_path() . '/authserver.uuid', $uuid);
            }
            AuthAPI::registerWithAuthServer(AuthAPI::$MODULE_TSGUI, $uuid);
        }
        $this->comment('App Registered successfully');
    }
}
