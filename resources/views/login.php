<!doctype html>
<html ng-app="TopengUI">
<head>
    <title>Topeng UI</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Topeng UI">

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,300,100,700,900' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- inject:css -->
    <link rel="stylesheet" href="css/lib/getmdl-select.min.css">
    <link rel="stylesheet" href="css/lib/nv.d3.css">
    <link rel="stylesheet" href="css/application.css">
    <!-- endinject -->
    <link href="css/style.css" rel="stylesheet" />

    <!-- inject:js -->
    <script src="js/d3.js"></script>
    <script src="js/getmdl-select.min.js"></script>
    <script src="js/material.js"></script>
    <script src="js/nv.d3.js"></script>

    <script type="text/javascript" src="node_modules/angular/angular.min.js"></script>
    <script type="text/javascript" src="js/jquery.min.js"></script>
<!--    <script type="text/javascript" src="js/bootstrap.min.js"></script>-->
    <script type="text/javascript" src="js/login.js"></script>
</head>
<body>
    <div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
        <div class="mdl-layout__header-row">
            <div class="logo pull-left">
                <a href="javascript:void(0);"><img src="/images/topeng.png?1" width="103px" /></a>
            </div>
            <div class="mdl-layout-spacer"></div>
        </div>
        <main class="mdl-layout__content mdl-color--grey-100">
            <div class="mdl-card mdl-shadow--2dp general-form" action="#">
                <div class="mdl-card__title">
                    <h2>Login</h2>
                    <div class="mdl-card__subtitle">Please login to continue</div>
                </div>
                <div class="mdl-card__supporting-text">
                    <form name="psgLoginForm" class="form" id="login_form" method="post" novalidate>
                        <div class="form__article">
                            <div class="mdl-grid">
                                <div class="mdl-cell mdl-cell--7-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input type="text" class="mdl-textfield__input" id="userName" name="login_username" required>
                                    <label class="mdl-textfield__label" for="userName">Username *</label>
                                    <span class="login_username mdl-textfield__error">Required *</span>
                                </div>
                                <div class="mdl-cell mdl-cell--7-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input type="password" class="mdl-textfield__input" id="password" name="login_password" required>
                                    <label class="mdl-textfield__label" for="password">Password *</label>
                                    <span class="login_password mdl-textfield__error">Required *</span>
                                </div>
                                <div class="mdl-cell mdl-cell--7-col mdl-textfield mdl-js-textfield mdl-textfield--floating-label"></div>
                            </div>
                        </div>
                        <input class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored-blue" name="login_submit" type="submit" value="Login" />
                        <div class="form__action">
                            <a class="forgot_pass" href="javascript:void(0);">Forgot Password ?</a>
<!--                            <input class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored" type="reset" value="Cancel" />-->
                        </div>
                    </form>
                    <br/>
                    <div id="loginProgress" class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
                </div>
            </div>
        </main>
    </div>
    <script type="text/javascript">
        var TopengLogin;
        (function(angular) {
            TopengLogin = angular.module('TopengUI', []);
        })(window.angular);
    </script>
</body>
</html>