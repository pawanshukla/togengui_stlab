<!doctype html>
<html ng-app="TopengUI">
<head>
    <title>Topeng UI</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Add to homescreen for Chrome on Android -->
    <meta name="mobile-web-app-capable" content="yes">
    <!-- Add to homescreen for Safari on iOS -->
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-title" content="Topeng UI">

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,500,300,100,700,900' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- inject:css -->
    <link rel="stylesheet" href="css/lib/getmdl-select.min.css">
    <link rel="stylesheet" href="css/lib/nv.d3.css">
    <link rel="stylesheet" href="css/application.css">
    <!-- endinject -->
    <link href="css/autocomplete.css" rel="stylesheet" />
    <link href="node_modules/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />

    <!-- inject:js -->
    <script src="js/d3.js"></script>
    <script src="js/getmdl-select.min.js"></script>
    <script src="js/material.js"></script>
    <script src="js/nv.d3.js"></script>
<!--    <script src="js/aes-js.js"></script>-->

    <script type="text/javascript" src="node_modules/angular/angular.min.js"></script>
    <script type="text/javascript" src="node_modules/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script type="text/javascript" src="node_modules/angular-resource/angular-resource.min.js"></script>
    <script type="text/javascript" src="node_modules/angular-cookies/angular-cookies.min.js"></script>
    <script type="text/javascript" src="node_modules/angular-filter/dist/angular-filter.min.js"></script>
    <script type="text/javascript" src="node_modules/ng-ip-address/src/ngIpAddress.js"></script>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/ui-bootstrap-tpls-0.12.1.min.js"></script>
    <script type="text/javascript" src="js/autocomplete.js"></script>
    <script type="text/javascript" src="node_modules/moment/min/moment.min.js"></script>

    <!--For Dashboards-->
    <script src="node_modules/chart.js/dist/Chart.min.js"></script>
    <script src="node_modules/angular-chart.js/dist/angular-chart.min.js"></script>
</head>
<body ng-cloak>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
    <header class="mdl-layout__header">
        <div class="mdl-layout__header-row">
            <div class="mdl-layout-title">
                <a href="javascript:void(0);"><img src="/images/topeng.png?1" class="logoimg" /></a>
            </div>
            <div class="mdl-layout-spacer"></div>
            <div class="mdl-layout-title">
                {{pageTitle}}
            </div>
            <div class="mdl-layout-spacer"></div>
            <div class="avatar-dropdown" id="icon">
                <span>Admin</span>
            </div>
            <ul class="mdl-menu mdl-list mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect mdl-shadow--2dp account-dropdown" for="icon">
                <li class="mdl-list__item mdl-list__item--two-line">
                    <span class="mdl-list__item-primary-content">
<!--                        <span class="material-icons mdl-list__item-avatar"></span>-->
                        <span>Admin</span>
                        <span class="mdl-list__item-sub-title">{{username || ''}}</span>
                    </span>
                </li>
                <li class="list__item--border-top"></li>
                <!--<li class="mdl-menu__item mdl-list__item">
                    <span class="mdl-list__item-primary-content">
                        <i class="material-icons mdl-list__item-icon">settings</i>
                        Settings
                    </span>
                </li>-->
                <li class="mdl-menu__item mdl-list__item" ng-click="doLogout()">
                    <span class="mdl-list__item-primary-content">
                        <i class="material-icons mdl-list__item-icon text-color--secondary">exit_to_app</i>
                        Logout
                    </span>
                </li>
            </ul>
        </div>
    </header>
    <div class="mdl-layout__drawer">
        <header><a href="javascript:void(0);"><img src="/images/topeng.png?1" class="logoimg" /></a></header>
        <nav class="mdl-navigation">
            <a class="mdl-navigation__link" menu-close ui-sref="dashboard" ng-class="{'mdl-navigation__link--current' : menuItem == 'dashboard'}">
                <i class="material-icons" role="presentation">dashboard</i>
                Dashboard
            </a>
            <a class="mdl-navigation__link" menu-close ui-sref="users" ng-class="{'mdl-navigation__link--current' : menuItem == 'user'}">
                <i class="material-icons" role="presentation">person</i>
                Users Management
            </a>
            <a class="mdl-navigation__link" menu-close ui-sref="groups" ng-class="{'mdl-navigation__link--current' : menuItem == 'group'}">
                <i class="material-icons" role="presentation">group</i>
                Groups Management
            </a>
            <a class="mdl-navigation__link" menu-close ui-sref="standards" ng-class="{'mdl-navigation__link--current' : menuItem == 'standard'}">
                <i class="material-icons" role="presentation">class</i>
                Standards
            </a>
            <a class="mdl-navigation__link" menu-close ui-sref="attributes" ng-class="{'mdl-navigation__link--current' : menuItem == 'attribute'}">
                <i class="material-icons" role="presentation">assignment</i>
                Attribute Management
            </a>
            <a class="mdl-navigation__link" menu-close ui-sref="policies" ng-class="{'mdl-navigation__link--current' : menuItem == 'policy'}">
                <i class="material-icons" role="presentation">gavel</i>
                Policy Management
            </a>
            <a class="mdl-navigation__link" menu-close ui-sref="configurations" ng-class="{'mdl-navigation__link--current' : menuItem == 'config'}">
                <i class="material-icons" role="presentation">settings</i>
                Configurations
            </a>
            <a class="mdl-navigation__link cursor-pointer" ng-click="controllerSubMenuLogs = !controllerSubMenuLogs" ng-class="{'mdl-navigation__link--current' : menuItem == 'logs'}">
                <i class="material-icons pull-left" role="presentation">description</i>
                <span class="menu-label pull-left">Logs</span>
            </a>
                <a class="mdl-navigation__link sub-link" menu-close ui-sref="audit" ng-show="controllerSubMenuLogs" ng-class="{'mdl-navigation__link--current' : menuItem == 'audit'}">
                    <i class="material-icons submenu-arrow pull-left" role="presentation">subdirectory_arrow_right</i>
                    <i class="material-icons pull-left" role="presentation">find_in_page</i>
                    Audit Log
                </a>
                <a class="mdl-navigation__link sub-link" menu-close ui-sref="accesslog" ng-show="controllerSubMenuLogs" ng-class="{'mdl-navigation__link--current' : menuItem == 'accesslog'}">
                    <i class="material-icons submenu-arrow pull-left" role="presentation">subdirectory_arrow_right</i>
                    <i class="material-icons pull-left" role="presentation">find_in_page</i>
                    Access Log
                </a>
                <a class="mdl-navigation__link sub-link" menu-close ui-sref="policylog" ng-show="controllerSubMenuLogs" ng-class="{'mdl-navigation__link--current' : menuItem == 'policylog'}">
                    <i class="material-icons submenu-arrow pull-left" role="presentation">subdirectory_arrow_right</i>
                    <i class="material-icons pull-left" role="presentation">find_in_page</i>
                    <span class="menu-label-2 pull-left">Policy Violations Log</span>
                </a>

            <a class="mdl-navigation__link cursor-pointer" menu-close ui-sref="services_management" ng-class="{'mdl-navigation__link--current' : menuItem == 'services-management'}">
                <i class="material-icons" role="presentation">settings</i>
                Services Management
            </a>
             <a class="mdl-navigation__link cursor-pointer" menu-close ui-sref="devices_registry" ng-class="{'mdl-navigation__link--current' : menuItem == 'devices-registry'}">
                <i class="material-icons" role="presentation">gavel</i>
                Device Registry
            </a>
             <a class="mdl-navigation__link cursor-pointer" menu-close ui-sref="gateways_management" ng-class="{'mdl-navigation__link--current' : menuItem == 'gateways-management'}">
                <i class="material-icons" role="presentation">device_hub</i>
                Gateway Management
            </a>
            <a class="mdl-navigation__link" menu-close ui-sref="sdk_management" ng-class="{'mdl-navigation__link--current' : menuItem == 'sdkmgmt'}">
                <i class="material-icons" role="presentation">find_in_page</i>
                SDK Management
            </a>
            <a class="mdl-navigation__link cursor-pointer" ng-click="controllerSubMenu = !controllerSubMenu" ui1-sref="deployment" ng-class="{'mdl-navigation__link--current' : menuItem == 'deployment'}">
                <i class="material-icons" role="presentation">storage</i>
                Deployment
            </a>
            <a class="mdl-navigation__link sub-link cursor-pointer" menu-close ui-sref="ipwhitelist" ng-show="controllerSubMenu" ng-class="{'mdl-navigation__link--current' : menuItem == 'ipwhitelist'}">
                <i class="material-icons submenu-arrow" role="presentation">subdirectory_arrow_right</i>
                <i class="material-icons hdmi" role="presentation">settings_input_hdmi</i>
                IP Whitelisting
            </a>
            <a class="mdl-navigation__link sub-link" menu-close ui-sref="uploadkeys" ng-show="controllerSubMenu && false" ng-class="{'mdl-navigation__link--current' : menuItem == 'uploadCustomkey'}">Upload Customer Key</a>

            <a class="mdl-navigation__link cursor-pointer" ng-click="controllerSubMenu2 = !controllerSubMenu2" ng-class="{'mdl-navigation__link--current' : menuItem == 'datamgmt'}">
                <i class="material-icons" role="presentation">sd_storage</i>
                Data Management
            </a>
			
                <a class="mdl-navigation__link sub-link" menu-close ui-sref="importldap" ng-show="controllerSubMenu2" ng-class="{'mdl-navigation__link--current' : menuItem == 'importldap'}">
                    <i class="material-icons submenu-arrow" role="presentation">subdirectory_arrow_right</i>
                    <i class="material-icons hdmi" role="presentation">import_export</i>
                    Import from LDAP
                </a>
			 <a class="mdl-navigation__link" menu-close ui-sref="onedrive" ng-class="{'mdl-navigation__link--current' : menuItem == 'onedrive'}">
                <i class="material-icons" role="presentation">settings</i>
                Onedrives Integration
            </a>

            <!-- <a class="mdl-navigation__link cursor-pointer" ng-click="controllerSubMenu3 = !controllerSubMenu3" ng-class="{'mdl-navigation__link--current' : menuItem == 'softdefinedperi'}">
                <i class="material-icons pull-left" role="presentation">device_hub</i>
                <span class="menu-label pull-left">Perimeter Controller</span>
            </a>
                <a class="mdl-navigation__link sub-link" menu-close ui-sref="onboard-devices" ng-show="controllerSubMenu3" ng-class="{'mdl-navigation__link--current' : menuItem == 'onbdevices'}">
                    <i class="material-icons submenu-arrow pull-left" role="presentation">subdirectory_arrow_right</i>
                    <i class="material-icons pull-left" role="presentation">devices_other</i>
                    <span class="menu-label-1 pull-left">Onboard Devices</span>
                </a>
                <a class="mdl-navigation__link sub-link" menu-close ui-sref="perimeter-user" ng-show="controllerSubMenu3" ng-class="{'mdl-navigation__link--current' : menuItem == 'periusers'}">
                    <i class="material-icons submenu-arrow" role="presentation">subdirectory_arrow_right</i>
                    <i class="material-icons" role="presentation">person_pin</i>
                    Perimeter User
                </a>
                <a class="mdl-navigation__link sub-link" menu-close ui-sref="perimeter-gateways" ng-show="controllerSubMenu3" ng-class="{'mdl-navigation__link--current' : menuItem == 'perigateway'}">
                    <i class="material-icons submenu-arrow pull-left" role="presentation">subdirectory_arrow_right</i>
                    <i class="material-icons pull-left" role="presentation">swap_vertical_circle</i>
                    <span class="menu-label-2 pull-left">Perimeter Gateways</span>
                </a>
                <a class="mdl-navigation__link sub-link" menu-close ui-sref="perimeter-controller" ng-show="controllerSubMenu3" ng-class="{'mdl-navigation__link--current' : menuItem == 'pericontroller'}">
                    <i class="material-icons submenu-arrow pull-left" role="presentation">subdirectory_arrow_right</i>
                    <i class="material-icons pull-left" role="presentation">developer_board</i>
                    <span class="menu-label-2 pull-left">Permissions</span>
                </a>
                <a class="mdl-navigation__link sub-link cursor-pointer" menu-close ui-sref="perimeter-accesslog" ng-show="controllerSubMenu3" ng-class="{'mdl-navigation__link--current' : menuItem == 'perimeter-accesslog'}">
                    <i class="material-icons submenu-arrow pull-left" role="presentation">subdirectory_arrow_right</i>
                    <i class="material-icons pull-left" role="presentation">find_in_page</i>
                    Access Log
                </a> -->
        </nav>
    </div>
    <main class="mdl-layout__content" ng-class="{'no-overflow1' : overFlow, 'no-overflow' : overFlow2}">
        <div id="header"></div>
        <div class="mdl-grid mdl-grid--no-spacing">
            <div class="mdl-grid mdl-cell mdl-cell--12-col-desktop mdl-cell--12-col-tablet mdl-cell--4-col-phone mdl-cell--top">
                <ui-view></ui-view>
            </div>
        </div>
    </main>
    <div class="whole_page_mask" ng-show="showMask">
        <div class="mdl-spinner mdl-js-spinner is-active"></div>
    </div>
</div>
<!--<footer id="footer">
    <p class="text-center">Designed and Developed by <a href="javascript:void(0);">author name</a> </p>
</footer>-->
<script type="text/javascript" src="js/app.js?1"></script>
<script type="text/javascript" src="js/routes.js"></script>
<script type="text/javascript" src="js/service.js?1"></script>
<script type="text/javascript" src="js/login-controller.js"></script>
<script type="text/javascript" src="js/dashboard-controller.js"></script>
<script type="text/javascript" src="js/user-controller.js"></script>
<script type="text/javascript" src="js/group-controller.js"></script>
<script type="text/javascript" src="js/policy-controller.js?1"></script>
<script type="text/javascript" src="js/standard-controller.js"></script>
<script type="text/javascript" src="js/attribute-controller.js"></script>
<script type="text/javascript" src="js/audit-controller.js"></script>
<script type="text/javascript" src="js/ipwhitelist-controller.js"></script>
<script type="text/javascript" src="js/sdk-controller.js"></script>
<script type="text/javascript" src="js/devicesRegistry-controller.js"></script>
<script type="text/javascript" src="js/servicesManagement-controller.js"></script>
<script type="text/javascript" src="js/gatewaysManagement-controller.js"></script>
<script type="text/javascript" src="js/import-ldap-controller.js"></script>
<!-- <script type="text/javascript" src="js/perimeter-controller.js"></script> -->
<script type="text/javascript" src="js/onedrive-controller.js"></script>
</body>
</html>